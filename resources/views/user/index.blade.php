@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12 mb-20">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6  content-center ">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-full">USER
                </h1>
                <a href="{{ route('user.create') }}" class="mt-1 ml-5">
                    <button type="button"
                        class="rounded-full bg-[#195243] px-3.5 py-2 mt-4 text-sm font-semibold text-white ring-gray-300">
                        Tambah User
                    </button>
                </a>
            </div>
            <!-- <div class="flex items-center gap-x-3 pb-6 ">
              <label for="photo" class="block text-sm font-bold text-black">NAMA PENGGUNA</label>
              <input type="text" name="first-ame" id="first-name" class="block w-full p-2 rounded-full border-0 py-1.5 text-black shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6">
              <button type="button" class="inline-flex items-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                Cari
              </button>
            </div> -->
            <div
                class="block w-full bg-white p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                <table class="min-w-full divide-y divide-gray-300 ">
                    <thead>
                        <tr>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-bold uppercase">Nama</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-bold uppercase">Username</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-bold uppercase">Level</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-bold uppercase">Bidang</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-bold uppercase">Jabatan</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-bold uppercase">Pengaturan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($selects as $data)
                            <tr class="border-b transition duration-300 ease-in-out">
                                <td class="px-3 py-3.5 text-sm text-black">{{ $data->name }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->username }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center uppercase">{{ $data->level }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center uppercase">{{ $data->bidang }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center uppercase">{{ $data->subbidang }}</td>
                                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center">
                                    <!-- <a href="{{ route('user.edit', $data->id) }}" class="mr-2">
                    <button type="button" class="inline-flex items-center rounded-full bg-[#195243] px-3.5 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                      Ubah
                    </button>
                  </a> -->
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('user.destroy', $data->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="inline-flex items-center rounded-full bg-red-700 px-2.5 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            Hapus
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                                Data belum tersedia! tambah {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                            </div>
                        @endforelse

                        <!-- More plans... -->
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
