@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight tracking-tight text-gray-900">PENGATURAN - Tambah User</h1>
            </div>

            <form action="{{ route('pengaturan.update', $pengaturan->id) }}" method="POST" enctype="multipart/form-data"
                class="-mx-4 ring-1 ring-gray-300 sm:mx-0 sm:rounded-lg bg-white p-6 mb-10">
                @csrf
                @method('PUT')

                <div class="space-y-12">
                    <div class="m grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div class="sm:col-span-3">
                            <label for="nama" class="block text-sm font-medium leading-6 text-gray-900">Nama</label>
                            <div class="mt-2">
                                <input type="text" name="nama" id="nama" autocomplete="family-name"
                                    value="{{ old('name', $pengaturan->name) }}"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="username" class="block text-sm font-medium leading-6 text-gray-900">Username</label>
                            <div class="mt-2">
                                <input type="text" name="username" id="username" autocomplete="given-name"
                                    value="{{ old('username', $pengaturan->username) }}"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="password" class="block text-sm font-medium leading-6 text-gray-900">Password</label>
                            <div class="mt-2">
                                <input type="text" name="password" id="password"
                                    value="{{ old('password', $pengaturan->password) }}"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="leveling" class="block text-sm font-medium leading-6 text-gray-900">Leveling</label>
                            <div class="mt-2">
                                <input type="text" name="level" id="level"
                                    value="{{ old('level', $pengaturan->level) }}"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="bidang" class="block text-sm font-medium leading-6 text-gray-900">Bidang</label>
                            <div class="mt-2">
                                <input type="text" name="bidang" id="bidang"
                                    value="{{ old('bidang', $pengaturan->bidang) }}"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 flex items-center justify-end gap-x-2">
                    <button type="submit"
                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-[#195243]">Simpan</button>
                    <button
                        class="rounded-md bg-red-700 px-5 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-red-700">Batal</button>
                </div>
            </form>

        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
