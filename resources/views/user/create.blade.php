@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight mt-5 tracking-tight text-gray-900">PENGATURAN - Tambah User</h1>
            </div>

            <form action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data"
                class="-mx-4 mt-10 ring-1 ring-gray-300 sm:mx-0 sm:rounded-lg bg-white p-6 mb-10">
                @csrf

                <div class="space-y-12">
                    <div class="m grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div class="sm:col-span-3">
                            <label for="nip" class="block text-sm font-medium leading-6 text-gray-900">NIP</label>
                            <div class="mt-2">
                                <input type="text" name="nip" id="nip" autocomplete="family-name"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="nama" class="block text-sm font-medium leading-6 text-gray-900">Nama</label>
                            <div class="mt-2">
                                <input type="text" name="nama" id="nama" autocomplete="family-name"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="username" class="block text-sm font-medium leading-6 text-gray-900">Username</label>
                            <div class="mt-2">
                                <input type="text" name="username" id="username" autocomplete="given-name"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="password" class="block text-sm font-medium leading-6 text-gray-900">Password</label>
                            <div class="mt-2">
                                <input type="text" name="password" id="password"
                                    class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="leveling" class="block text-sm font-medium leading-6 text-gray-900">Level</label>
                            <div class="mt-2">
                                <select id="slct1" name="slct1"
                                    class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"
                                    onchange="level(this.id, 'slct2')">
                                    <option value=""></option>
                                    <option value="uptd">Uptd</option>
                                    <option value="bidang">Bidang</option>
                                    <option value="verifikator">Verifikator</option>
                                    <option value="Biro Hukum">Biro Hukum</option>
                                </select>
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="leveling" class="block text-sm font-medium leading-6 text-gray-900">Bidang</label>
                            <div class="mt-2">
                                <select id="slct2" name="slct2"
                                    class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"
                                    onchange="bidang(this.id, 'slct3')">
                                </select>
                            </div>
                        </div>

                        <div class="sm:col-span-3">
                            <label for="leveling" class="block text-sm font-medium leading-6 text-gray-900">Jabatan</label>
                            <div class="mt-2">
                                <select id="slct3" name="slct3"
                                    class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 flex items-center justify-end gap-x-2">
                    <button type="submit"
                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm ">SIMPAN</button>
                    <button
                        class="rounded-md bg-red-600 px-5 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-red-600">BATAL</button>
                </div>
            </form>

        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>

    <script type="text/javascript">
        function level(s1, s2) {
            var s1 = document.getElementById(s1);
            var s2 = document.getElementById(s2);

            s2.innerHTML = "";

            if (s1.value == "uptd") {
                var optionArray = [
                    "|",
                    "Kota Samarinda|Kota Samarinda",
                    "Kota Bontang|Kota Bontang",
                    "Kota Balikpapan|Kota Balikpapan",
                    "Kabupaten Kubar|Kabupaten Kubar",
                    "Kabupaten Paser|Kabupaten Paser",
                    "Kabupaten Penajam Paser Utara|Kabupaten Penajam Paser Utara",
                    "Kabupaten Kutim|Kabupaten Kutim",
                    "Kabupaten Kukar|Kabupaten Kukar",
                    "Kabupaten Berau|Kabupaten Berau"
                ];
            } else if (s1.value == "bidang") {
                var optionArray = [
                    "|",
                    "Sekretaris|Sekretaris",
                    "Perencanaan & Pengembangan Sistem Informasi Pendapatan Pembina|Perencanaan & Pengembangan Sistem Informasi Pendapatan Pembina",
                    "Penerimaan Bukan Pajak Daerah Pembina Tingkat 1|Penerimaan Bukan Pajak Daerah Pembina Tingkat 1",
                    "Pajak Daerah Pembina Tingkat 1|Pajak Daerah Pembina Tingkat 1",
                    "Pengendalian & Evaluasi Pendapatan Pembina Tingat 1|Pengendalian & Evaluasi Pendapatan Pembina Tingat 1"
                ];
            } else if (s1.value == "verifikator") {
                var optionArray = [
                    "|",
                    "Staf|Staf"
                ];
            } else if (s1.value == "Biro Hukum") {
                var optionArray = [
                    "|",
                    "Staf Biro|Staf Biro"
                ];
            }

            for (var option in optionArray) {
                var pair = optionArray[option].split("|");
                var newOption = document.createElement("option");
                newOption.value = pair[0];
                newOption.innerHTML = pair[1];
                s2.options.add(newOption);
            }
        }

        function bidang(s2, s3) {
            var s2 = document.getElementById(s2);
            var s3 = document.getElementById(s3);

            s3.innerHTML = "";

            if (s2.value == "Sekretaris") {
                var optionArray = [
                    "Kabag Sekretaris Pembina|Kabag Sekretaris Pembina",
                    "Kasubag Umum Pembina|Kasubag Umum Pembina",
                    "Analisis Keuangan Pemerintah Pusat dan Daerah Ahli Muda Penata Tingkat 1|Analisis Keuangan Pemerintah Pusat dan Daerah Ahli Muda Penata Tingkat 1"
                ];
            } else if (s2.value == "Perencanaan & Pengembangan Sistem Informasi Pendapatan Pembina") {
                var optionArray = [
                    "Kabid Perencanaan & Pengembangan Sistem Informasi Pendapatan Pembina|Kabid Perencanaan & Pengembangan Sistem Informasi Pendapatan Pembina",
                    "Kasubid Analisis Pengembangan Pendapatan & Pelayanan Pembina|Kasubid Analisis Pengembangan Pendapatan & Pelayanan Pembina",
                    "Kasubid Perencanaan Anggaran Pendapatan & Pengembangan SIstem Informasi Pendapatan Penatan Tingkat 1|Kasubid Perencanaan Anggaran Pendapatan & Pengembangan SIstem Informasi Pendapatan Penatan Tingkat 1",
                ];
            } else if (s2.value == "Penerimaan Bukan Pajak Daerah Pembina Tingkat 1") {
                var optionArray = [
                    "Kabid Penerimaan Bukan Pajak Daerah Pembina Tingkat 1|Kabid Penerimaan Bukan Pajak Daerah Pembina Tingkat 1",
                    "Kasubbid Bagi Hasil Pajak & Bukan Pajak Penata Tingkat 1|Kasubbid Bagi Hasil Pajak & Bukan Pajak Penata Tingkat 1",
                    "Kasubbid Retribusi Daerah Pembina|Kasubbid Retribusi Daerah Pembina",
                ];
            } else if (s2.value == "Pajak Daerah Pembina Tingkat 1") {
                var optionArray = [
                    "Kabid Pajak Daerah Pembina Tingkat 1|Kabid Pajak Daerah Pembina Tingkat 1",
                    "Kasubbid Pkb dan Bbkkb Penata Tingkat 1|Kasubbid Pkb dan Bbkkb Penata Tingkat 1",
                    "Kasubbid Pendapatan Administrasi dan Ratan Pajak Pembina|Kasubbid Pendapatan Administrasi dan Ratan Pajak Pembina",
                ];
            } else if (s2.value == "Pengendalian & Evaluasi Pendapatan Pembina Tingat 1") {
                var optionArray = [
                    "Kabid Pengendalian & Evaluasi Pendapatan Pembina Tingkat 1|Kabid Pengendalian & Evaluasi Pendapatan Pembina Tingkat 1",
                    "Kasubbid Pengendalian & Evaluasi Pendapatan Pembina|Kasubbid Pengendalian & Evaluasi Pendapatan Pembina",
                    "Kasubbid Kajuan Hukun & Perundang Undangan Penata|Kasubbid Kajuan Hukun & Perundang Undangan Penata",
                ];
            } else if (s2.value == "Staf") {
                var optionArray = [
                    "|",
                    "Staf 1 (Perda, Pergub, SK Gub)|Staf 1 (Perda, Pergub, SK Gub)",
                    "Staf 2 (SK Sekda, SK Kaban)|Staf 2 (SK Sekda, SK Kaban)"
                ];
            } else if (s2.value == "Staf Biro") {
                var optionArray = [
                    "|",
                    "Staf|Staf"
                ];
            } else {
                var optionArray = [
                    "|",
                    "Kepala TU|Kepala TU",
                    "Kasubag TU|Kasubag TU"
                ];
            }

            for (var option in optionArray) {
                var pair = optionArray[option].split("|");
                var newOption = document.createElement("option");
                newOption.value = pair[0];
                newOption.innerHTML = pair[1];
                s3.options.add(newOption);
            }
        }
    </script>
@endsection
