@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-full">
                    RESUME</h1>
            </div>

            <!-- <form method="get"  class="">

                                                                                                      <div class="col-span-full flex">
                                                                                                      <label for="jenis" class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis File</label>
                                                                                                          <select id="jenis" name="jenis" value="user" class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                                                                              <option value="Perda">Perda</option>
                                                                                                              <option value="Pergub">Pergub</option>
                                                                                                              <option value="SK Gubernur">SK Gubernur</option>
                                                                                                              <option value="SK Sekda">SK Sekda</option>
                                                                                                              <option value="SK Kaban">SK Kaban</option>
                                                                                                          </select>
                                                                                                      </div>

                                                                                                      <div class="col-span-full flex mt-5 ">
                                                                                                      <label for="jenis" class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Bulan</label>
                                                                                                          <select id="tahun" name="tahun" value="user" class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                                                                            <option value="1">Januari</option>
                                                                                                            <option value="2">Februari</option>
                                                                                                            <option value="3">Maret</option>
                                                                                                            <option value="4">April</option>
                                                                                                            <option value="5">Mei</option>
                                                                                                            <option value="6">Juni</option>
                                                                                                            <option value="7">Juli</option>
                                                                                                            <option value="8">Agustus</option>
                                                                                                            <option value="9">September</option>
                                                                                                            <option value="10">Oktober</option>
                                                                                                            <option value="11">Nopember</option>
                                                                                                              <option value="12">Desember</option>
                                                                                                          </select>
                                                                                                      </div>
                                                                                                      <button type="submit" name="aksi" value="cari" class="bg-[#195243] text-white w-full  text-center rounded mt-5 text-sm leading-6 font-bold pl-6 py-3">
                                                                                                        CARI
                                                                                                      </button> -->
            <!-- <button type="submit" name="aksi" value="download" class="bg-[#195243] text-white w-full  text-center rounded mt-5 text-sm leading-6 font-bold pl-6 py-3">
                                                                                                        DOWNLOAD
                                                                                                      </button> -->

            <!-- </form> -->
            <div
                class="block w-full bg-white p-2 rounded-md border-0 mt-5 mb-5  py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                <table class="min-w-full divide-y divide-gray-300 ">
                    <thead>
                        <tr>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">Judul</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Deskripsi</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Jenis</th>
                            {{-- <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Sumber</th> --}}
                            <th scope="col"
                                class="px-3 py-3.5 text-center text-sm font-semibold text-black whitespace-nowrap">Status
                            </th>
                            {{-- <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Tgl Terbit --}}
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Lihat
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($selects as $data)
                            <tr class="border-b transition duration-300 ease-in-out">
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                                <td class="px-3 py-3.5 text-sm text-black">{{ $data->deskripsi }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->jenis }}</td>
                                {{-- <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->bidang }}</td> --}}
                                <td class="relative py-3.5  text-sm font-bold  ">
                                    <a
                                        class="text-sm text-center uppercase whitespace-nowrap
              {{ $data->status == 'Verifikasi'
                  ? 'bg-orange-600 text-white rounded-full px-2 py-3'
                  : ($data->status == 'Selesai'
                      ? 'bg-[#195243] text-white rounded-full px-4 py-3'
                      : ($data->status == ''
                          ? 'bg-[#195243] text-white rounded-full px-4 py-3'
                          : ($data->status == 'Sedang Diproses'
                              ? 'bg-blue-900 text-white rounded-full px-4 py-3'
                              : ($data->status == 'Berkas Dikembalikan'
                                  ? 'bg-red-700 text-white rounded-full p-3'
                                  : '')))) }}">
                                        {{ $data->status == 'Verifikasi' ? 'Ajukan Verifikasi' : ($data->status == '' ? 'Selesai' : $data->status) }}
                                    </a>
                                </td>
                                {{-- <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->tgl_dittd }}</td> --}}
                                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium  justify-center">
                                    <form method="get" action="{{ route('resumeDetail') }}" class="items-center">
                                        <input type="text" name="id_usulan" id="id_usulan" value="{{ $data->id }}"
                                            hidden />
                                        <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}"
                                            hidden />
                                        <input type="text" name="status" id="status" value="{{ $data->status }}"
                                            hidden />
                                        <button type="submit"
                                            class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            <?php
                if ($data->status == 'Berkas Dikembalikan') {
                ?>
                                            {{ Session::get('level') == 'verifikator' ? 'Detail' : (Session::get('level') == 'Biro Hukum' ? 'Detail' : 'Detail') }}
                                            <?php
                } else {
                ?>
                                            {{ Session::get('level') == 'verifikator' ? 'Detail' : (Session::get('level') == 'Biro Hukum' ? 'Detail' : 'Detail') }}
                                            <?php
                }
                ?>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                                <?php
                if (Session::get('level') == 'verifikator') {
                ?>
                                Semua usulan telah di Detail
                                <?php
                } else {
                ?>
                                Data belum tersedia! tambah
                                {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                                <?php
                }
                ?>
                            </div>
                        @endforelse

                        <!-- More plans... -->
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
