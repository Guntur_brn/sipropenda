@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-md">
                    USULAN</h1>
            </div>

            @forelse ($datas as $datas)
                <div class="mb-6">
                    <dl class="gap-0 flex rounded-l">
                        <?php if($_GET['status']==null) { ?>
                        <?php } else { ?>
                        <div
                            class="overflow-hiden rounded-md
           bg-[#195243] p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Upload<br>Berkas</dt>
                        </div>
                        <div class="overflow-hiden bg-[#195243] h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            bg-[#195243]
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Proses<br>Verifikasi</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
          bg-gray-400 h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-3">
                            <dt class="text-xl text-center w-full  font-bold text-white">Berkas<br>Dikembalikan</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Sedang<br>Diproses</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-24 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">SELESAI</dt>
                        </div>
                        <?php } ?>
                    </dl>
                </div>


                <div class="space-y-12">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                        <div class="col-span-full flex">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Nomor
                                Surat</label>
                            <input value="{{ old('nip', $datas->nomor_surat) }}" disabled type="text" name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>
                        <div class="col-span-full flex">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                Berkas</label>
                            <input value="{{ old('nip', $datas->judul) }}" disabled type="text" name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>

                        <div class="col-span-full flex">
                            <label for="nama"
                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>

                            <?php
                    if (old('nip', Session::get('level')) == 'admin') {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas) }}"></iframe>
                            </div>
                            <?php
                    } else if (old('nip', $datas->status) == 'Berkas Dikembalikan') {
                    ?>
                            <input id="file" type="file" name="file" />
                            <?php
                    } else if (old('nip', $datas->berkas_perbaikan) == null) {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas) }}"></iframe>
                            </div>
                            <?php
                    }  else {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas_perbaikan) }}"></iframe>
                            </div>
                            <?php
                     }
                    ?>

                        </div>
                    </div>
                </div>
            @empty
            @endforelse

            <div class="mt-10  w-full gap-x-2 mb-11 ">
                <a
                    href="{{ (Session::get('level') == 'admin' or Session::get('subbidang') == 'Kab') ? '\resume' : '\resume' }}">
                    <button type="button"
                        class="w-full rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                        Kembali
                    </button>
                </a>
            </div>

        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
