<table class="table min-w-full divide-y divide-gray-300 ">
    <thead>
        <tr>
            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">Judul</th>
            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Deskripsi</th>
            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Jenis</th>
            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Status</th>
            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Catatan</th>
            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Pengaturan</th>
        </tr>
    </thead>
    <tbody>
        @forelse($selects as $data)
            <tr class="border-b transition duration-300 ease-in-out">
                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                <td class="px-3 py-3.5 text-sm text-black">{{ $data->deskripsi }}</td>
                <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->jenis }}</td>
                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-bold flex justify-center">
                    <a
                        class="text-sm text-center uppercase
              {{ $data->status == 'Verifikasi'
                  ? 'bg-orange-600 text-white rounded-full px-12 py-3'
                  : ($data->status == 'Selesai'
                      ? 'bg-[#195243] text-white rounded-full px-14 py-3'
                      : ($data->status == 'Sedang Diproses'
                          ? 'bg-blue-900 text-white rounded-full px-14 py-3'
                          : ($data->status == 'Berkas Dikembalikan'
                              ? 'bg-red-700 text-white rounded-full p-3'
                              : ''))) }}">
                        {{ $data->status == 'Verifikasi' ? 'Ajukan Verifikasi' : $data->status }}
                    </a>
                </td>
                <td class="px-3 py-3.5 text-sm text-black">{{ $data->catatan }}</td>
                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center">
                    <form method="get" action="{{ route('usulanDetail') }}" class="items-center">
                        <input type="text" name="id_usulan" id="id_usulan" value="{{ $data->id }}" hidden />
                        <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}" hidden />
                        <input type="text" name="status" id="status" value="{{ $data->status }}" hidden />
                        <button type="submit"
                            class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                            <?php
                if ($data->status == 'Berkas Dikembalikan') {
                ?>
                            {{ Session::get('level') == 'verifikator' ? 'Verifikasi' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Upload Ulang') }}
                            <?php
                } else {
                ?>
                            {{ Session::get('level') == 'verifikator' ? 'Verifikasi' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Detail') }}
                            <?php
                }
                ?>
                        </button>
                    </form>
                </td>
            </tr>
        @empty
            <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                <?php
                if (Session::get('level') == 'verifikator') {
                ?>
                Semua usulan telah di Verifikasi
                <?php
                } else {
                ?>
                Data belum tersedia! tambah {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                <?php
                }
                ?>
            </div>
        @endforelse

        <!-- More plans... -->
    </tbody>
</table>
