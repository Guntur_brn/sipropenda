<!doctype html>
<html>

<head>

    <!-- icon -->
    <link rel="icon" href="images/logo.png" type="image/x-icon">

    <title>SIPROPENDA</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    session(['level' => '']);
    session(['bidang' => '']);
    session(['nip' => '']);
    session(['nama' => '']);
    session(['subbidang' => '']);
    session(['sub' => '']);
    ?>
    <!--
  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ],
  }
  ```
-->
    <!--
  This example requires updating your template:

  ```
  <html class="h-full bg-white">
  <body class="h-full">
  ```
-->

    <div class="flex relatife">
        <img class="absolute h-full w-full object-cover " src="/images/gedung_bapenda.jpeg" alt=""></img>
        <div class="absolute h-full w-full object-cover bg-[#195243] opacity-60 "></div>
        <div class="absolute mt-16 ml-16 flex">
            <div class="mx-auto ">
                <div>
                    <form action="{{ url('proses_login') }}" method="POST" enctype="multipart/form-data"
                        class=" -mx-4 sm:mx-0 sm:rounded-lg bg-white p-6 mb-10">
                        @csrf
                        <div class="grid ">
                            <img src="/images/logoNamaP.png" class="w-80 place-self-center ">
                        </div>

                        <div class="mb-2 -mt-10">
                            <label for="username"
                                class="block text-base font-medium leading-3 text-gray-900">Username</label>
                            <div class="mt-2">
                                <input id="username" name="username" type="text" autocomplete="email" required
                                    class="px-3 text-base block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="mb-2">
                            <label for="password"
                                class="block text-base font-medium leading-6 text-gray-900">Password</label>
                            <div class="mt-2">
                                <input id="password" name="password" type="password" required
                                    class="px-3 text-base block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                            </div>
                        </div>

                        <div class="mt-5">
                            <button type="submit"
                                class="flex w-full justify-center rounded-md bg-[#195243] px-5 py-3 text-base font-semibold leading-6 text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">LOGIN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div
            class="absolute left-1/3 ml-5 top-28 -mt-7 font-black text-6xl text-white drop-shadow-[0_10px_10px_rgba(0,0,0,0.6)] leading-none border-s-4 pl-5">
            SISTEM <br>
            INFORMASI <br>
            PANTAU <br>
            PENERBITAN <br>
            PRODUK
            HUKUM<br>
            PENDAPATAN <br>
            DAERAH

            <div class="mt-5 flex gap-2">
                <a href="/dashboardPublic?cari="
                    class="flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">
                    <button type="button" class="">DASHBOARD</button>
                </a>
                <button type="submit"
                    class="openModal flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">PANDUAN</button>
            </div>

        </div>
        <div
            class="absolute right-2  w-60  top-16 font-black text-6xl drop-shadow-[0_10px_10px_rgba(0,0,0,10)] leading-none pl-5">
            <img class="absolute h-30 w-20" src="/images/logo_prov_kaltim.png" alt=""></img>
        </div>
        <div
            class="absolute right-2 bg-white w-1/6  mr-16 h-12  top-44 rounded-full drop-shadow-[0_10px_10px_rgba(0,0,0,1)] font-black text-6xl leading-none ">
            <img class="absolute h-30 px-3 w-auto " src="/images/logo_bapenda_kaltim.png" alt=""></img>
        </div>

        <div aria-labelledby="modal-title" role="dialog" aria-modal="true" id="interestModal" class="invisible">
            <div class="absolute h-full w-full object-cover bg-slate-600 opacity-70"></div>
            <div class="absolute w-2/3 h-2/3 m-auto  left-0 right-0 -top-20 bottom-0">
                <div
                    class="absolute relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl m-auto left-0 right-0 top-0 bottom-0">
                    <div class="bg-white p-5 sm:p-6 sm:pb-4">
                        <div class="sm:flex sm:items-start">
                            <div class="mt-3 w-full text-center sm:text-left">
                                <h3 class="font-semibold leading-6 text-gray-900 text-2xl" id="modal-title">DOWNLOAD
                                    PANDUAN</h3>
                                <div class="mt-2">

                                    <?php
                                    $pengupload = DB::table('panduan')
                                        ->latest()
                                        ->get(); ?>

                                    <table class="w-full divide-y divide-gray-300 mt-10">
                                        <thead>
                                            <tr>
                                                <th scope="col"
                                                    class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                                    Level</th>
                                                <th scope="col"
                                                    class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                                    Panduan</th>
                                                <th scope="col"
                                                    class="px-3 py-3.5 text-center text-sm font-semibold text-black">
                                                    Pengaturan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($pengupload as $data)
                                                <tr class="border-b transition duration-300 ease-in-out">
                                                    <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">
                                                        {{ $data->level == 'Biro Umum' ? 'Biro Hukum' : ($data->level == 'Kasub / Kabid' ? 'Kepala' : ($data->level == 'Kepala TU' ? 'Kasubag' : $data->level)) }}
                                                    </td>
                                                    <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">
                                                        {{ $data->panduan }}</td>
                                                    <td
                                                        class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center gap-2">
                                                        <form method="get" action="/downloadPanduan"
                                                            class="items-center">
                                                            <input type="text" name="id" id="id"
                                                                value="{{ $data->panduan }}" hidden />
                                                            <button type="submit"
                                                                class="inline-flex items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                                                Download
                                                            </button>
                                                        </form>

                                                    </td>
                                                    </td>
                                                </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>

                                    <button type="button"
                                        class="closeModal w-full mt-10 items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                        KEMBALI
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.openModal').on('click', function(e) {

                $('#interestModal').removeClass('invisible');

            });

            $('.closeModal').on('click', function(e) {

                $('#interestModal').addClass('invisible');

            });

        });
    </script>

</body>

</html>
