<!doctype html>
<html>

<head>

    <!-- icon -->
    <link rel="icon" href="images/logo.png" type="image/x-icon">

    <title>SIPROPENDA</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    session(['level' => '']);
    session(['bidang' => '']);
    session(['nip' => '']);
    session(['nama' => '']);
    session(['subbidang' => '']);
    session(['sub' => '']);
    ?>

    <div class="flex relatife ">
        <img class="absolute h-full w-full object-cover " src="/images/gedung_bapenda.jpeg" alt=""></img>
        <div class="absolute h-full w-full object-cover bg-[#195243] opacity-60 "></div>
        <div
            class="z-40 flex lg:absolute lg:flex-row flex-col-reverse items-center justify-center place-content-center w-full h-full lg:py-16 xl:py-20 2xl:py-24 ">
            <div class="mt-5 lg:hidden w-2/3 flex flex-col flex gap-2 b-0 lg:flex">
                <a href="/dashboardPublic?cari="
                    class="flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">
                    <button type="button" class="">DASHBOARD</button>
                </a>
                <button type="submit"
                    class="openModal flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">PANDUAN</button>
            </div>

            <div class="bg-white w-2/3 lg:w-1/4 h-fit p-4 flex flex-col items-center rounded-lg lg:mr-5 ">
                <img src="/images/logoNamaP.png" class="w-4/6  lg:w-5/6 -mt-3 place-self-center ">
                <form action="{{ url('proses_login') }}" method="POST" enctype="multipart/form-data"
                    class=" -mx-4 sm:mx-0 sm:rounded-lg p-6 lg:mb-10 ">
                    @csrf
                    <div class="mb-2  -mt-10 lg:-mt-20">
                        <label for="username"
                            class="block lg:text-sm xl:text-base 2xl:text-2xl font-medium leading-3 text-black">Username</label>
                        <div class="mt-2">
                            <input id="username" name="username" type="text" autocomplete="email" required
                                class="px-3 lg:text-sm xl:text-base 2xl:text-2xl block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>
                    </div>

                    <div class="mb-2 ">
                        <label for="password"
                            class="block lg:text-sm xl:text-base 2xl:text-2xl font-medium leading-3 text-black">Password</label>
                        <div class="mt-2">
                            <input id="password" name="password" type="password" required
                                class="px-3 lg:text-sm xl:text-base 2xl:text-2xl block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>
                    </div>

                    <div class="mt-5">
                        <button type="submit"
                            class="flex w-full justify-center rounded-md bg-[#195243] px-5 py-3 lg:text-sm xl:text-base 2xl:text-2xl font-semibold leading-6 text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">LOGIN</button>
                    </div>
                </form>
            </div>

            <div
                class="lg:w-1/3 w-full h-full lg:my-0 my-8 flex items-center place-content-center lg:border-s-4 lg:pl-5 ">
                <div
                    class=" font-black self-center text-2xl lg:text-left text-center h-auto lg:text-5xl xl:text-6xl 2xl:text-7xl text-white drop-shadow-[0_10px_10px_rgba(0,0,0,0.6)] leading-none ">
                    SISTEM <br class="hidden lg:flex">
                    INFORMASI <br>
                    PANTAU <br class="hidden lg:flex">
                    PENERBITAN <br>
                    PRODUK
                    HUKUM<br>
                    PENDAPATAN <br class="hidden lg:flex">
                    DAERAH

                    <div class="mt-5 flex gap-2 b-0 hidden lg:flex">
                        <a href="/dashboardPublic?cari="
                            class="flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">
                            <button type="button" class="">DASHBOARD</button>
                        </a>
                        <button type="submit"
                            class="openModal flex w-full justify-center rounded-md bg-white px-5 py-3 text-base font-semibold leading-6 text-[#195243] shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">PANDUAN</button>
                    </div>
                </div>

            </div>

            <div
                class=" lg:w-1/6 lg:place-self-start lg:self-start gap-3 lg:ml-20 w-full h-full flex justify-center lg:items-start lg:justify-start items-center lg:flex lg:flex-col lg:px-0 px-5 lg:mt-0 mt-5 ">
                <div class="flex place-content-center  h-fit ">
                    <img class="lg:h-auto lg:w-1/3 w-[30px] h-fit drop-shadow-[0_10px_10px_rgba(0,0,0,10)]"
                        src="/images/logo_prov_kaltim.png"></img>
                </div>
                <div class="h-fit  w-fit">
                    <img class="lg:h-auto px-3 h-[30px] lg:w-fit bg-white h-12   rounded-full drop-shadow-[0_10px_10px_rgba(0,0,0,1)] font-black"
                        src="/images/logo_bapenda_kaltim.png" alt=""></img>
                </div>
            </div>

        </div>
        <div aria-labelledby="modal-title" role="dialog" aria-modal="true" id="interestModal"
            class="fixed z-40 invisible h-full  flex items-center justify-center w-full p-8">
            <div class="absolute h-full w-full object-cover bg-gray-800 opacity-70"></div>
            <div class=" px-5  lg:px-0 w-full lg:w-2/3 m-auto  left-0 right-0">
                <div
                    class="absolute relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl m-auto left-0 right-0 ">
                    <div class="bg-white p-5 sm:p-6 sm:pb-4">
                        <div class="sm:flex sm:items-start">
                            <div class="mt-3 w-full text-left">
                                <h3 class="font-semibold leading-6 text-gray-900 text-2xl" id="modal-title">DOWNLOAD
                                    PANDUAN
                                </h3>
                                <div class="mt-2">

                                    <?php
                                    $pengupload = DB::table('panduan')->latest()->get(); ?>

                                    <table class="w-full divide-y divide-gray-300 mt-10">
                                        <thead>
                                            <tr>
                                                <th scope="col"
                                                    class="lg:flex hidden py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                                    Level
                                                </th>
                                                <th scope="col"
                                                    class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                                    Panduan</th>
                                                <th scope="col"
                                                    class="px-3 py-3.5 text-center text-sm font-semibold text-black">
                                                    Pengaturan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($pengupload as $data)
                                                <tr class="border-b transition duration-300 ">
                                                    <td class="lg:flex hidden relative py-3 pl-5 pr-3 text-sm w-2/12">
                                                        {{ $data->level == 'Biro Umum' ? 'Biro Hukum' : ($data->level == 'Kasub / Kabid' ? 'Kepala' : ($data->level == 'Kepala TU' ? 'Kasubag' : $data->level)) }}
                                                    </td>
                                                    <td class="relative py-3 pl-5 pr-3 text-sm w-2/12 w-full">
                                                        Panduan
                                                        {{ $data->level == 'Biro Umum' ? 'Biro Hukum' : ($data->level == 'Kasub / Kabid' ? 'Kepala' : ($data->level == 'Kepala TU' ? 'Kasubag' : $data->level)) }}
                                                        SI PRO PENDA
                                                    </td>
                                                    <td
                                                        class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center gap-2">
                                                        <form method="get" action="/downloadPanduan"
                                                            class="items-center">
                                                            <input type="text" name="id" id="id"
                                                                value="{{ $data->panduan }}" hidden />
                                                            <button type="submit"
                                                                class="inline-flex items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                                                Download
                                                            </button>
                                                        </form>

                                                    </td>
                                                    </td>
                                                </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>

                                    <button type="button"
                                        class="closeModal w-full mt-10 items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                        KEMBALI
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php
    $pengupload = DB::table('panduan')->latest()->get(); ?>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.openModal').on('click', function(e) {

                $('#interestModal').removeClass('invisible');

            });

            $('.closeModal').on('click', function(e) {

                $('#interestModal').addClass('invisible');

            });

        });
    </script>



</body>

</html>
