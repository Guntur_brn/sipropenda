@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight mt-5 tracking-tight text-gray-900">PENGATURAN - Tambah Produk
                    Hukum</h1>
            </div>

            <form action="{{ route('produkHukum.store') }}" method="POST" enctype="multipart/form-data"
                class="-mx-4 ring-1 ring-gray-300 sm:mx-0 sm:rounded-md bg-white p-6 mb-10">
                @csrf

                <div class="space-y-12">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">

                        <div class="col-span-full flex">
                            <label for="noSurat" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Nomor
                                Surat</label>
                            <input type="text" name="noSurat" id="noSurat" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                Produk Hukum</label>
                            <input type="text" name="judul" id="judul" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="nama"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>
                            <input id="file" type="file" name="file" accept=".doc, .docx, .pdf" />
                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                            <textarea type="text" name="deskripsi" id="deskripsi" rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"></textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                File</label>
                            <select id="jenis" name="jenis" value="user"
                                class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                <option value="Perda">Perda</option>
                                <option value="Pergub">Pergub</option>
                                <option value="SK Gubernur">SK Gubernur</option>
                                <option value="SK Sekda">SK Sekda</option>
                                <option value="SK Kaban">SK Kaban</option>
                            </select>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                Upload</label>
                            <input value="{{ date('Y-m-d') }}" disabled type="text" name="tgl" id="tgl"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                    </div>
                </div>

                <div class="mt-6 flex items-center justify-end gap-x-2">
                    <button type="submit"
                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">Simpan</button>
                </div>


            </form>


        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
