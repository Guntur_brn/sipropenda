@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12 mb-20">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6  content-center ">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-full">
                    PRODUK HUKUM</h1>
                <a href="{{ route('produkHukum.create') }}" class="mt-1 ml-5">
                    <button type="button"
                        class="rounded-full bg-[#195243] px-3.5 py-2 mt-4 text-sm font-semibold text-white ring-gray-300">
                        Tambah Produk Hukum
                    </button>
                </a>
            </div>
            <div class="flex items-center gap-x-3 pb-6 ">
                <label for="photo" class="block text-sm font-bold text-black">Nama Produk Hukum</label>
                <input type="text" name="first-ame" id="first-name"
                    class="block w-full p-2 rounded-full border-0 py-1.5 text-black shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6">
                <button type="button"
                    class="inline-flex items-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                    Cari
                </button>
            </div>
            <div
                class="block w-full bg-white p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                <table class="min-w-full divide-y divide-gray-300 ">
                    <thead>
                        <tr>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">No Surat
                            </th>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">Judul</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Deskripsi</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Tgl Upload
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Jenis</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Pengaturan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($selects as $data)
                            <tr class="border-b transition duration-300 ease-in-out items-center">
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->nomor_surat }}</td>
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                                <td class="px-3 py-3.5 text-sm text-black">{{ $data->deskripsi }}</td>
                                <td class="px-3 py-3.5 text-sm text-black">{{ substr($data->created_at, 0, 10) }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->jenis }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center">
                                    <div class="flex ">
                                        <form method="get" action="{{ route('detailProdukHukum') }}" class="items-center">
                                            <input type="text" name="id" id="id" value="{{ $data->id }}"
                                                hidden />
                                            <button type="submit"
                                                class="inline-flex items-center rounded-full bg-[#195243] px-2.5 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                                Ubah
                                            </button>
                                        </form>
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                            action="{{ route('produkHukum.destroy', $data->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit"
                                                class="inline-flex items-center rounded-full bg-red-700 px-2.5 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                                Hapus
                                            </button>
                                        </form>
                                    </div>
                                </td>
                                </td>
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                                <?php
                if (Session::get('level') == 'verifikator') {
                ?>
                                Semua usulan telah di Verifikasi
                                <?php
                } else {
                ?>
                                Data belum tersedia! tambah {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                                <?php
                }
                ?>
                            </div>
                        @endforelse

                        <!-- More plans... -->
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
