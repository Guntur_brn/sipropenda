@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <m an class="bg-bgColor h-screen px-2">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight tracking-tight text-gray-900">DETAIL PRODUK HUKUM</h1>
            </div>

            @forelse ($datas as $usulan)
            @empty
            @endforelse


            <form action="{{ route('usuln.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="space-y-12 mt-10">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                        <div class="col-spanfull flex">
                            <label for="noSurat" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">No
                                Surat</label>
                            <input value="{{ old('nip', $usulan->nomor_surat) }}" type="text" ame="noSurat"
                                id="noSurat" autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>

                        <div class="col-span-full fle x">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                Berkas</label>
                            <input value="{{ old('nip', $usulan->judul) }}" type="text" name="judul" id="judul"
                                autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>

                        <div class="col-span-full flex">

                            <label for="nama"
                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>


                            <?php
                    if (old('nip', Session::get('level')) == 'admin') {
                    ?>
                            <div s tyle="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $usulan->berkas) }}"></iframe>
                            </div> <?php
                  
  } else if (old('nip', $usulan->status) == 'Berkas Dikembalikan') {
                    ?>
                            <div style="cle ar:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $produkHukum->berkas) }}"></iframe>
                            </div>
                            <input id="file" type="file" name="file" />
                            <?php
                    } else if (old('nip', $usulan->berkas_perbaikan) != '') {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $usulan->berkas_perbaikan) }}"></iframe>
                            </div>
                            <?php
                    }  else {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $usulan->berkas) }}"></iframe>
                            </div>
                            <?php
                     }
                    ?>

                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                            <textarea type="text" name="detail" id="detail"
                                {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? '' : '' }} rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->deskripsi) }}</textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                File</label>
                            <input value="{{ old('nip', $usulan->jenis) }}" type="text" name="judul" id="judul"
                                autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                Upload</label>
                            <input value="{{ substr(old('nip', $usulan->updated_at), 0, 10) }}" type="text" name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                    </div>
                </div>

                <div class="mt-10 flex items-center justify-end gap-x-2 mb-11">
                    <button type="submit"
                        class=  "rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                        Ubah
                    </button>
                    <a
                        href="{{ (Session::get('level') == 'admin' or Session::get('subbidang') == 'Kab') ? '\resume' : '\resume' }}">
                        <button type="button"
                            class=  "rounded-md bg- px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                            Kembali
                        </button>
                    </a>
                </div>
            </form>
        </div>
        </div>
        </main>

        <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('content');
        </script>
    @endsection
