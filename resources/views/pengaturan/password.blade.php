@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <main class="bg-bgColor h-screen px-12 mb-20">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6  content-center ">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-full">
                    PENGATURAN</h1>
            </div>

            <form action="{{ route('update-password') }}" method="POST">
                <div class="my-6 grid grid-cols-3 gap-4">
                    @csrf
                    <div class="col-span-full flex">
                        <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Password
                            Baru</label>
                        <input type="text" name="new_password" id="new_password" autocomplete="given-name"
                            class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                    </div>
                    <div id="result"></div>

                    <div class="col-span-full flex">
                        <button type="submit"
                            class="mt-3 w-full rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                            Ubah Password
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
