<!doctype html>
<html>

<head>

    <!-- icon -->
    <link rel="icon" href="images/logo.png" type="image/x-icon">

    <title>SIPROPENDA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="h-100 w-full bg-gradient-to-r px-5 lg:px-20 my-10 from-[#128768]  to-[#0e7056] ">

    <div class="text-4xl lg:text-7xl text-white font-semibold mt-10 mb-8">
        DASHBOARD
    </div>

    <div class="text-2xl text-white font-semibold mt-10 mb-4">
        PRODUK HUKUM PENDAPATAN YANG TERSEDIA
    </div>

    <?php
    use App\Models\Perda;
    use App\Models\Sksekda;
    use Illuminate\Support\Facades\DB;
    use Kyslik\ColumnSortable\Sortable;

    $perda = DB::table('file_perda')->get()->where('status', '=', 'Selesai')->count();
    $perda = DB::table('file_produk_hukum')->get()->where('jenis', '=', 'Perda')->count() + $perda;
    $pergub = DB::table('file_pergub')->get()->where('status', '=', 'Selesai')->count();
    $pergub = DB::table('file_produk_hukum')->get()->where('jenis', '=', 'Pergub')->count() + $pergub;
    $skgub = DB::table('file_skgub')->get()->where('status', '=', 'Selesai')->count();
    $skgub = DB::table('file_produk_hukum')->get()->where('jenis', '=', 'SK Gubernur')->count() + $skgub;
    $skkaban = DB::table('file_skkaban')->get()->where('status', '=', 'Selesai')->count();
    $skkaban = DB::table('file_produk_hukum')->get()->where('jenis', '=', 'SK Kaban')->count() + $skkaban;
    $sksekda = DB::table('file_sksekda')->get()->where('status', '=', 'Selesai')->count();
    $sksekda = DB::table('file_produk_hukum')->get()->where('jenis', '=', 'SK Sekda')->count() + $sksekda;
    $produkhukum = DB::table('file_produk_hukum')->get()->count();

    $total = $perda + $pergub + $skgub + $skkaban + $sksekda;

    $file_perda = DB::table('file_perda');
    $file_pergub = DB::table('file_pergub');
    $file_skgub = DB::table('file_skgub');
    $file_skkaban = DB::table('file_skkaban');
    $file_sksekda = DB::table('file_sksekda');

    $file_selects = DB::table('file_skgub')->union($file_perda)->union($file_pergub)->union($file_skgub)->union($file_skkaban)->union($file_sksekda)->get();

    ?>

    <div class="flex">
        <div
            class="rounded-2xl w-full lg:w-2/5 h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-4xl font-semibold leading-12">TOTAL<br>PRODUK<br>HUKUM</div>
            <div class="text-8xl font-bold ">{{ $total }}</div>
        </div>
    </div>

    <div class="w-full mt-10 flex grid grid-cols-2 lg:grid-cols-5 gap-3">
        <a href="{{ route('resumePublic', 'jenis=PD') }}"
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold ">{{ $perda }}</div>
                <div class="text-xl lg:text-3xl font-semibold">PERATURAN<br>DAERAH</div>
            </div>
        </a>
        <a href="{{ route('resumePublic', 'jenis=PG') }}"
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $pergub }}</div>
                <div class="text-xl lg:text-3xl font-semibold">PERATURAN<br>GUBERNUR</div>
            </div>
        </a>
        <div
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $skgub }}</div>
                <div class="text-xl lg:text-3xl font-semibold">SK<br>GUBERNUR</div>
            </div>
        </div>
        <div
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $sksekda }}</div>
                <div class="text-xl lg:text-3xl font-semibold">SK<br>SEKDA</div>
            </div>
        </div>
        <div
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $skkaban }}</div>
                <div class="text-xl lg:text-3xl font-semibold">SK<br>KABAN</div>
            </div>
        </div>
    </div>

    <div class="text-2xl text-white font-semibold mt-10 mb-4">
        TOTAL STATUS USULAN
    </div>

    <div class="w-full mt-10 flex grid grid-cols-2 lg:grid-cols-3 gap-3">
        <div
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold mt-7">
                    {{ $file_selects->where('status', '=', 'Sedang Diproses')->count() }}</div>
                <div class="text-xl lg:text-3xl font-semibold">BERKAS<br>SEDANG<br>DIPROSES</div>
            </div>
        </div>
        <div
            class="rounded-2xl w-full h-46 bg-white px-7  py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $file_selects->where('status', '=', 'Verifikasi')->count() }}
                </div>
                <div class="text-xl lg:text-3xl font-semibold">BERKAS<br>DIVERIFIKASI</div>
            </div>
        </div>
        <div
            class="rounded-2xl w-full h-46 bg-white px-7 py-4 flex justify-between items-center text-[#195243]  shadow-2xl">
            <div class="text-center w-full">
                <div class="text-6xl mb-3 font-bold">{{ $total }}</div>
                <div class="text-xl lg:text-3xl font-semibold">BERKAS<br>DITERBITKAN</div>
            </div>
        </div>
    </div>

    <form method="get" action="/dashboardPublic" class="">
        <div class="flex flex-col lg:flex-row items-center gap-3  mt-14 ">
            <div class="flex gap-2 w-full">
                <label for="photo" class="block text-sm font-bold text-white">JUDUL PERATURAN</label>
                <input type="text" name="cari" id="cari" value="{{ $_GET['cari'] }}"
                    class="block w-full text-center p-2 rounded-full border-0 py-1.5 text-black shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6">
            </div>
            <div class="flex gap-2 w-full lg:w-fit">
                <button type="submit"
                    class="inline-flex w-full flex justify-center items-center text-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                    Cari
                </button>
                <a href="/dashboardPublic?cari=" class="w-full">
                    <div class="flex items-center gap-x-3  ">
                        <button type="button"
                            class="inline-flex w-full flex justify-center text-center items-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                            reset
                        </button>
                    </div>
                </a>
            </div>
        </div>
    </form>

    <div class="w-full overflow-auto bg-white rounded-2xl mt-5">
        <table class="w-full overflow-auto divide-y divide-white bg-white  rounded-2xl ">
            <thead>
                <tr>
                    <th scope="col"
                        class="py-3 pl-5 pr-3 text-left text-sm font-bold text-[#195243] text-center uppercase">
                        {{-- @sortablelink('judul')</th> --}}
                        @sortablelink('judul', 'judul', ['cari' => 'cari'])</th>
                    <th scope="col"
                        class="py-3 pl-5 pr-3 text-left text-sm font-bold text-[#195243] text-center uppercase">
                        @sortablelink('No Surat')</th>
                    <th scope="col"
                        class="py-3 pl-5 pr-3 text-center text-sm font-bold text-[#195243] text-center uppercase">
                        @sortablelink('Deskripsi')</th>
                    <th scope="col"
                        class="px-3 py-3.5 text-center text-sm font-bold text-[#195243] text-center uppercase">
                        @sortablelink('Jenis')</th>
                    <th scope="col"
                        class="px-3 py-3.5 text-center text-sm font-bold text-[#195243] text-center uppercase">
                        @sortablelink('Lihat')</th>
                </tr>
            </thead>
            <?php
            if ($_GET['cari'] == 'cari' or $_GET['cari'] == '') {
                $tpl_perda = DB::table('file_perda')->where('status', '=', 'Selesai')->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi');
                $tpl_pergub = DB::table('file_pergub')->where('status', '=', 'Selesai')->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi');
                $tpl_fileProdukHukum1 = DB::table('file_produk_hukum')->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')->where('jenis', '=', 'Perda');
                $tpl_fileProdukHukum2 = DB::table('file_produk_hukum')->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')->where('jenis', '=', 'Pergub');
                $selects = Perda::sortable()->where('status', '=', 'Selesai')->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')->union($tpl_pergub)->union($tpl_fileProdukHukum1)->union($tpl_fileProdukHukum2)->orderBy('jenis', 'ASC')->get();
            } else {
                // $tpl_perda = DB::table('file_perda')
                //     ->where('status', '=', 'Selesai')
                //     ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi');
                // $tpl_pergub = DB::table('file_pergub')
                //     ->where('status', '=', 'Selesai')
                //     ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi');
                // $tpl_fileProdukHukum1 = DB::table('file_produk_hukum')
                //     ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                //     ->where('jenis', '=', 'Perda');
                // $tpl_fileProdukHukum2 = DB::table('file_produk_hukum')
                //     ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                //     ->where('jenis', '=', 'Pergub');

                // $selects = Perda::sortable()
                //     ->where('status', '=', 'Selesai')
                //     ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                //     ->union($tpl_pergub)
                //     ->union($tpl_fileProdukHukum1)
                //     ->union($tpl_fileProdukHukum2)
                //     ->get();
                $tpl_perda = DB::table('file_perda')
                    ->where('status', '=', 'Selesai')
                    ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                    ->where('judul', 'like', '%' . $_GET['cari'] . '%');
                $tpl_pergub = DB::table('file_pergub')
                    ->where('status', '=', 'Selesai')
                    ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                    ->where('judul', 'like', '%' . $_GET['cari'] . '%');
                $tpl_fileProdukHukum1 = DB::table('file_produk_hukum')
                    ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                    ->where('jenis', '=', 'Perda')
                    ->where('judul', 'like', '%' . $_GET['cari'] . '%');
                $tpl_fileProdukHukum2 = DB::table('file_produk_hukum')
                    ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                    ->where('jenis', '=', 'Pergub')
                    ->where('judul', 'like', '%' . $_GET['cari'] . '%');

                $selects = Perda::sortable()
                    ->where('status', '=', 'Selesai')
                    ->select('id', 'judul', 'jenis', 'status', 'nomor_surat', 'deskripsi')
                    ->where('judul', 'like', '%' . $_GET['cari'] . '%')
                    ->union($tpl_pergub)
                    ->union($tpl_fileProdukHukum1)
                    ->union($tpl_fileProdukHukum2)
                    ->orderBy('jenis', 'ASC')
                    ->get();
            }
            ?>

            <tbody class="bg-grey-light " style="height: 50vh; overflow-y:auto;">
                @forelse($selects as $data)
                    <tr class="border-b transition duration-300 ease-in-out">
                        <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                        <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->nomor_surat }}</td>
                        <td class="px-3 py-3.5 text-sm text-black text-left">{{ $data->deskripsi }}a</td>
                        <td class="px-3 py-3.5 text-sm text-black text-center uppercase">{{ $data->jenis }}</td>
                        <td class="px-3 py-3.5 text-sm text-black text-center">
                            <?php if (substr($data->jenis, 0, 2) != 'SK') { ?>
                            <form method="get" action="{{ route('resumeDetailPublic') }}" class="items-center">
                                <input type="text" name="id_usulan" id="id_usulan" value="{{ $data->id }}"
                                    hidden />
                                <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}"
                                    hidden />
                                <input type="text" name="status" id="status" value="{{ $data->status }}"
                                    hidden />
                                <button type="submit"
                                    class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                    Detail</button>
                            </form>
                            <?php } else { ?>
                            <form method="get" action="/" class="items-center">
                                <button type="submit"
                                    class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                    Detail</button>
                            </form>
                            <?php } ?>
                        </td>

                    </tr>

                @empty
                @endforelse
                <!-- More plans... -->
            </tbody>
        </table>
    </div>
    <a href="/"
        class="text-[#195243] bg-white block  text-center rounded-xl mt-10 text-sm leading-6 font-bold pl-6 py-3">
        KEMBALI
    </a>

</body>

</html>
