@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight tracking-tight text-gray-900">DETAIL PANDUAN</h1>
            </div>

            @forelse ($datas as $usulan)
            @empty
            @endforelse



            <form action="{{ route('usulan.update', $usulan->id) }}" method="POST" enctype="multipart/form-data"
                class="mt-10">
                @csrf
                @method('PUT')

                <div class="space-y-12">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                        <div class="col-span-full flex">
                            <label for="level" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">level
                                User</label>
                            <input value="{{ old('nip', $usulan->level) }}" disabled type="text" name="level"
                                id="level" autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>

                        <div class="col-span-full flex">
                            <label for="nama"
                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $usulan->panduan) }}"></iframe>
                            </div>
                        </div>

                        <hr />
                    </div>
                </div>

                <div class="mt-10 flex items-center justify-end gap-x-2 mb-11">
                    <a href="/panduan">
                        <button type="button"
                            class=  "rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                            Kembali
                        </button>
                    </a>
                </div>
            </form>
        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
