@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12 mb-20">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6  content-center ">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-full">
                    PANDUAN</h1>
            </div>
            <div
                class="block w-full bg-white p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                <table class="min-w-full divide-y divide-gray-300 ">
                    <thead>
                        <tr>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                @sortablelink('Level')</th>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">
                                @sortablelink('Panduan')</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">
                                @sortablelink('Pengaturan')</th>
                        </tr>
                    </thead>
                    <?php
                    $pengupload = DB::table('panduan')
                        ->latest()
                        ->get(); ?>

                    <tbody>
                        @forelse($selects as $data)
                            <tr class="border-b transition duration-300 ease-in-out">
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">
                                    {{ $data->level == 'Biro Umum' ? 'Biro Hukum' : ($data->level == 'Kasub / Kabid' ? 'Kepala' : ($data->level == 'Kepala TU' ? 'Kasubag' : $data->level)) }}
                                </td>
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12"> </td>
                                <td
                                    class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center gap-2">
                                    <form method="get" action="{{ route('detailPanduan') }}" class="items-center">
                                        <input type="text" name="id" id="id" value="{{ $data->id }}"
                                            hidden />
                                        <button type="submit"
                                            class="inline-flex items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            Lihat
                                        </button>
                                    </form>
                                    <form method="get" action="/downloadPanduan" class="items-center">
                                        <input type="text" name="id" id="id" value="{{ $data->panduan }}"
                                            hidden />
                                        <button type="submit"
                                            class="inline-flex items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            Download
                                        </button>
                                    </form>
                                    <form method="get" action="{{ route('detailPanduan') }}" class="items-center">
                                        <input type="text" name="id" id="id" value="{{ $data->id }}"
                                            hidden />
                                        <input type="text" name="ubah" id="ubah" value="ubah" hidden />
                                        <button type="submit"
                                            class="inline-flex items-center rounded-full bg-[#195243] px-6 py-1.5 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            Ubah
                                        </button>
                                    </form>

                                </td>
                                </td>
                            </tr>
                        @empty
                        @endforelse

                        <!-- More plans... -->
                    </tbody>
                </table>
                <!-- {!! $selects->appends(\Request::except('page'))->render() !!} -->
            </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
