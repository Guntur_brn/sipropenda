@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-3xl font-bold leading-tight tracking-tight text-gray-900">
                    {{ isset($_GET['ubah']) ? 'UBAH' : 'DETAIL' }} PANDUAN</h1>
            </div>

            @forelse ($datas as $usulan)
            @empty
            @endforelse



            <form action="{{ route('panduan.update', $usulan->id) }}" method="POST" enctype="multipart/form-data"
                class="mt-10">
                @csrf
                @method('PUT')

                <div class="">
                    <div class="">

                        <div class="col-span-full flex mb-5">
                            <label for="nama"
                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Panduan
                                Lama</label>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $usulan->panduan) }}"></iframe>
                            </div>
                        </div>

                        <?php
                    if (isset($_GET['ubah'])){
                ?>
                        <div class="col-span-full flex mt-5">
                            <label for="nama"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Panduan
                                Baru</label>
                            <input id="file" type="file" name="file" accept=".doc, .docx, .pdf">
                        </div>
                        <div class="mt-6 flex items-center justify-end gap-x-2 mb-2">
                            <button type="submit"
                                class="w-full py-3 rounded-md bg-[#195243] px-3 py-2 text-md font-semibold text-white shadow-sm  ">
                                SIMPAN
                            </button>
                        </div>
                        <?php
                    }else{}
                ?>

                        <div class=" block  gap-x-2  mb-10">
                            <a href="/panduan" class="block">
                                <button type="button"
                                    class="w-full py-3 rounded-md bg-[#195243] px-3 py-2 text-md font-semibold text-white shadow-sm  ">
                                    KEMBALI
                                </button>
                            </a>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
