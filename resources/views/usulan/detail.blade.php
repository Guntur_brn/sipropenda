@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')

    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-md">DETAIL
                    USULAN</h1>
            </div>

            @forelse ($datas as $datas)
                <div class="mb-6">
                    <dl class="gap-0 flex rounded-l">
                        <?php if($_GET['status']==null) { ?>
                        <?php } else { ?>
                        <div
                            class="overflow-hiden rounded-md
           bg-[#195243] p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Upload<br>Berkas</dt>
                        </div>
                        <div class="overflow-hiden bg-[#195243] h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            bg-[#195243]
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Proses<br>Verifikasi</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
          bg-gray-400 h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-3">
                            <dt class="text-xl text-center w-full  font-bold text-white">Berkas<br>Dikembalikan</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-20 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">Sedang<br>Diproses</dt>
                        </div>
                        <div
                            class="overflow-hiden
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-24 mt-12">
                        </div>
                        <div
                            class="overflow-hidden rounded-md
            {{ old('nip', $datas->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $datas->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                            <dt class="text-xl text-center w-full  font-bold text-white">SELESAI</dt>
                        </div>
                        <?php } ?>
                    </dl>
                </div>


                <div class="space-y-12">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                        <div class="col-span-full flex">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                Berkas</label>
                            <input value="{{ old('nip', $datas->judul) }}" disabled type="text" name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                        </div>

                        <div class="col-span-full flex">
                            <label for="nama"
                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>

                            <?php
                    if (old('nip', Session::get('level')) == 'admin') {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas) }}"></iframe>
                            </div>
                            <?php
                    } else if (old('nip', $datas->status) == 'Berkas Dikembalikan') {
                    ?>
                            <input id="file" type="file" name="file" />
                            <?php
                    } else if (old('nip', $datas->berkas_perbaikan) == null) {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas) }}"></iframe>
                            </div>
                            <?php
                    }  else {
                    ?>
                            <div style="clear:both">
                                <iframe id="viewer" frameborder="0" scrolling="no" width="800" height="800"
                                    src="/assetes/{{ old('berkas', $datas->berkas_perbaikan) }}"></iframe>
                            </div>
                            <?php
                     }
                    ?>

                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                            <textarea type="text" name="detail" id="detail"
                                {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->deskripsi) }}</textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                File</label>
                            <input value="{{ old('nip', $datas->jenis) }}" type="text" disabled name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                Upload</label>
                            <input value="{{ substr(old('nip', $datas->updated_at), 0, 10) }}" type="text" disabled
                                name="judul" id="judul" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <?php
                  if (Session::get('level') != 'verifikator') {
                ?>


                        <div class="col-span-full flex">
                            <label for="bidang"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                            <input value="{{ old('nip', $datas->status) }}" type="text" disabled name="judul"
                                id="judul" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                            <textarea type="text" name="detail" id="detail"
                                {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->catatan) }}</textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan Biro
                                Hukum</label>
                            <textarea type="text" name="detail" id="detail"
                                {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->berkas_catatan) }}</textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Link
                                Terkait</label>
                            <textarea type="text" name="detail" id="detail"
                                {{ old('nip', $datas->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->link_terkait) }}</textarea>
                        </div>

                        <?php if ((($datas->status) == 'Selesai') && (Session::get('level')) == 'verifikator') { ?>


                        <?php
        if (old('nip', $datas->jenis) == 'SK Gubernur') {
        ?>
                        <form action="{{ route('skgub.update', $datas->id) }}" method="POST"
                            enctype="multipart/form-data">
                            <?php
        } else if (old('nip', $datas->jenis) == 'SK Sekda') {
        ?>
                            <form action="{{ route('sksekda.update', $datas->id) }}" method="POST"
                                enctype="multipart/form-data">
                                <?php
          }else if (old('nip', $datas->jenis) == 'SK Kaban') {
        ?>
                                <form action="{{ route('skkaban.update', $datas->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    <?php
          } else if (old('nip', $datas->jenis) == 'Perda') {
        ?>
                                    <form action="{{ route('perda.update', $datas->id) }}" method="POST"
                                        enctype="multipart/form-data">
                                        <?php
          } else if (old('nip', $datas->jenis) == 'Pergub') {
        ?>
                                        <form action="{{ route('pergub.update', $datas->id) }}" method="POST"
                                            enctype="multipart/form-data">
                                            <?php
            }
        ?>
                                            @csrf
                                            <div class="col-span-full flex">
                                                <label for="catatan"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Link
                                                    Terkait</label>
                                                <textarea type="text" name="link" id="link" rows="5"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->link_terkait) }}</textarea>
                                            </div>
                                            <button type="submit"
                                                class="w-full mt-5 rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                                                Upload Link
                                            </button>
                                            <!-- </form> -->

                                            <?php } else { ?>

                                            <?php } ?>

                                            <?php
                  } else {
                ?>

                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                                <select id="status" name="status" value="user"
                                                    class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                    <option value="Berkas Dikembalikan">Berkas Dikembalikan</option>
                                                    <option value="Sedang Diproses">Sedang Diproses</option>
                                                    <option value="Selesai">Selesai</option>
                                                </select>
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="catatan"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                                                <textarea type="text" name="catatan" id="catatan" rows="5"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $datas->catatan) }}</textarea>
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="nama"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>
                                                <input id="file" type="file" name="file" />
                                            </div>


                                            <?php
                  }
                ?>

                                            <hr />

                                            <?php
                    if (old('nip', $datas->status) == 'Berkas Dikembalikan') {
                    ?>

                                            <?php
                    } else {
                    ?>
                                            <hr />

                                            <div class="hidden sm:-my-px sm:flex py-2 justify-between">
                                                <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900">
                                                    Status Berkas</h1>
                                            </div>

                                            <div>
                                                <div
                                                    class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                    <input disabled {{ $datas->berkas_dikirim == 1 ? 'checked' : '' }}
                                                        type="checkbox"
                                                        class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                        Dikirim</label>
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">{{ $datas->tgl_dikirim }}</label>
                                                </div>

                                                <div
                                                    class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                    <input disabled
                                                        {{ $datas->status == 'Berkas Dikembalikan' ? 'checked' : '' }}
                                                        type="checkbox"
                                                        class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                        Dikembalikan</label>
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">{{ $datas->tgl_dikembalikan }}</label>
                                                </div>

                                                <div
                                                    class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                    <input disabled {{ $datas->berkas_diterima == 1 ? 'checked' : '' }}
                                                        type="checkbox"
                                                        class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                        Diterima</label>
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">{{ $datas->tgl_diterima }}</label>
                                                </div>

                                                <div
                                                    class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                    <input disabled {{ $datas->berkas_diteruskan == 1 ? 'checked' : '' }}
                                                        type="checkbox"
                                                        class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                        Diteruskan</label>
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">{{ $datas->tgl_diterima }}</label>
                                                </div>

                                                <div
                                                    class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                    <input disabled
                                                        {{ $datas->berkas_ditandatangani == 1 ? 'checked' : '' }}
                                                        type="checkbox"
                                                        class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                        Ditandatangani</label>
                                                    <label
                                                        class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">{{ $datas->tgl_dittd }}</label>
                                                </div>
                                            </div>

                                            <hr />

                                            <div class="hidden sm:-my-px sm:flex py-2 justify-between">
                                                <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900">
                                                    Pengupload Berkas</h1>
                                            </div>





                                            <?php
                                            $pengupload = DB::table('users')
                                                ->latest()
                                                ->where('nip', old('nip', $datas->nip))
                                                ->get(); ?>


                                            @forelse ($pengupload as $data)
                                                <div class="col-span-full flex">
                                                    <label for="bidang"
                                                        class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">NIP</label>
                                                    <input value="{{ old('nip', $data->nip) }}" type="text" disabled
                                                        name="judul" id="judul" autocomplete="given-name"
                                                        class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                </div>

                                                <div class="col-span-full flex">
                                                    <label for="bidang"
                                                        class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Nama</label>
                                                    <input value="{{ old('nip', $data->name) }}" type="text" disabled
                                                        name="judul" id="judul" autocomplete="given-name"
                                                        class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                </div>

                                                <div class="col-span-full flex">
                                                    <label for="bidang"
                                                        class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Level</label>
                                                    <input value="{{ old('nip', $data->level) }}" type="text" disabled
                                                        name="judul" id="judul" autocomplete="given-name"
                                                        class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                </div>

                                                <div class="col-span-full flex">
                                                    <label for="bidang"
                                                        class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Bidang</label>
                                                    <input value="{{ old('nip', $data->bidang) }}" type="text"
                                                        disabled name="judul" id="judul" autocomplete="given-name"
                                                        class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                </div>

                                                <div class="col-span-full flex">
                                                    <label for="bidang"
                                                        class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Sub
                                                        Bidang</label>
                                                    <input value="{{ old('nip', $data->subbidang) }}" type="text"
                                                        disabled name="judul" id="judul" autocomplete="given-name"
                                                        class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                </div>

                                            @empty
                                            @endforelse

                                            <?php
                    }
                    ?>
                    </div>
                </div>
            @empty
            @endforelse

            <div class="mt-10  w-full gap-x-2 mb-11 ">
                <a
                    href="{{ (Session::get('level') == 'admin' or Session::get('subbidang') == 'Kab') ? '\resume' : '\resume' }}">
                    <button type="button"
                        class="w-full rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                        Kembali
                    </button>
                </a>


                <?php
        if(Session::get('level') != "") {
          if (old('nip', $datas->jenis) == 'SK Gubernur') {
          ?>
                <form action="{{ route('skgub.destroy', $datas->id) }}" method="POST" enctype="multipart/form-data">
                    <?php
          } else if (old('nip', $datas->jenis) == 'SK Sekda') {
          ?>
                    <form action="{{ route('sksekda.destroy', $datas->id) }}" method="POST"
                        enctype="multipart/form-data">
                        <?php
            }else if (old('nip', $datas->jenis) == 'SK Kaban') {
          ?>
                        <form action="{{ route('skkaban.destroy', $datas->id) }}" method="POST"
                            enctype="multipart/form-data">
                            <?php
            } else if (old('nip', $datas->jenis) == 'Perda') {
          ?>
                            <form action="{{ route('perda.destroy', $datas->id) }}" method="POST"
                                enctype="multipart/form-data">
                                <?php
            } else if (old('nip', $datas->jenis) == 'Pergub') {
          ?>
                                <form action="{{ route('pergub.destroy', $datas->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    <?php
              }
          ?>

                                    @csrf
                                    @method('delete')
                                    <a href="/pergub/destroy/{{ $datas->id }}">
                                        <button type="submit"
                                            class="mt-3 w-full rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm  ">
                                            Batalkan Usulan
                                        </button>
                                    </a>
                                </form>
                                <?php
            }
          ?>
            </div>


        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>

@endsection
