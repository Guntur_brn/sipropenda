@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4 ">USULAN</h1>
            </div>
            <form method="get" action="{{ route('cariUsulanAdmin') }}" class="">
                <div class="flex items-center gap-x-3 pb-6 ">
                    <label for="photo" class="block text-sm font-bold text-black">JUDUL USULAN</label>
                    <input type="text" name="cari" id="cari"
                        class="block w-full p-2 rounded-full border-0 py-1.5 text-black shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6">
                    <button type="submit"
                        class="inline-flex items-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                        Cari
                    </button>
                </div>
            </form>
            <div
                class="block w-full bg-white p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                <table class="min-w-full divide-y divide-gray-300 ">
                    <thead>
                        <tr>
                            <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">Judul</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Deskripsi</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Jenis</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Status</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Catatan</th>
                            <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Pengaturan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($selects as $data)
                            <tr class="border-b transition duration-300 ease-in-out">
                                <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                                <td class="px-3 py-3.5 text-sm text-black">{{ $data->deskripsi }}</td>
                                <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->jenis }}</td>
                                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-bold flex justify-center">
                                    <a
                                        class="text-sm text-center uppercase
              {{ $data->status == 'Verifikasi'
                  ? 'bg-orange-600 text-white rounded-full px-12 py-3'
                  : ($data->status == 'Selesai'
                      ? 'bg-[#195243] text-white rounded-full px-14 py-3'
                      : ($data->status == 'Sedang Diproses'
                          ? 'bg-blue-900 text-white rounded-full px-14 py-3'
                          : ($data->status == 'Berkas Dikembalikan'
                              ? 'bg-red-700 text-white rounded-full p-3'
                              : ''))) }}">
                                        {{ $data->status == 'Verifikasi' ? 'Ajukan Verifikasi' : $data->status }}
                                    </a>
                                </td>
                                <td class="px-3 py-3.5 text-sm text-black">{{ $data->catatan }}</td>
                                <td class="relative py-3.5 pl-3 pr-4 text-center text-sm font-medium flex justify-center">
                                    <form method="get" action="{{ route('usulanDetail') }}" class="items-center">
                                        <input type="text" name="id_usulan" id="id_usulan" value="{{ $data->id }}"
                                            hidden />
                                        <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}"
                                            hidden />
                                        <input type="text" name="status" id="status" value="{{ $data->status }}"
                                            hidden />
                                        <button type="submit"
                                            class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                            {{ Session::get('level') == 'verifikator' ? 'Verifikasi' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Detail') }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                                <?php
                if (Session::get('level') == 'verifikator') {
                ?>
                                Semua usulan telah di Verifikasi
                                <?php
                } else {
                ?>
                                Data belum tersedia! tambah
                                {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                                <?php
                }
                ?>
                            </div>
                        @endforelse

                        <!-- More plans... -->
                    </tbody>
                </table>
            </div>
        </div>
        <div class="pb-20">
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
    <script type="text/javascript">
        function PreviewImage() {
            pdffile = document.getElementById("file").files[0];
            pdffile_url = URL.createObjectURL(pdffile);
            $('#viewer').attr('src', pdffile_url);
        }
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
@endsection
