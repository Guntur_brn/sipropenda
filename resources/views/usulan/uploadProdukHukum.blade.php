@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12 ">
        <div>
            <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
            <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-md">
                    {{ Session::get('level') == 'verifikator' ? 'VERIFIKASI USULAN' : (Session::get('level') == 'Biro Hukum' ? 'TERBITKAN USULAN' : '') }}
                </h1>
            </div>

            @forelse ($datas as $usulan)
            @empty
            @endforelse


            <?php
        if (old('nip', $usulan->jenis) == 'SK Gubernur') {
        ?>
            <form action="{{ route('skgub.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                skgub
                <?php
        } else if (old('nip', $usulan->jenis) == 'SK Sekda') {
        ?>
                <form action="{{ route('sksekda.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                    sksekda
                    <?php
          }else if (old('nip', $usulan->jenis) == 'SK Kaban') {
        ?>
                    <form action="{{ route('skkaban.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                        skkaban
                        <?php
          } else if (old('nip', $usulan->jenis) == 'Perda') {
        ?>
                        <form action="{{ route('perda.update', $usulan->id) }}" method="POST"
                            enctype="multipart/form-data">
                            perda
                            <?php
          } else if (old('nip', $usulan->jenis) == 'Pergub') {
        ?>
                            <form action="{{ route('pergub.update', $usulan->id) }}" method="POST"
                                enctype="multipart/form-data">
                                pergub
                                <?php
            }
        ?>
                                @csrf
                                @method('PUT')
                                <div class="space-y-12 mt-10">
                                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                                        <div class="col-span-full flex">
                                            <label for="nosurat"
                                                class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Nomor
                                                Surat</label>
                                            <input value="{{ old('nip', $usulan->nomor_surat) }}" type="text"
                                                name="nomor_surat" id="nomor_surat" autocomplete="given-name"
                                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                                            <input value="{{ old('nip', $usulan->status) }}" type="text" name="status"
                                                id="status" autocomplete="given-name"
                                                class="block w-full hidden  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                                            <input value="{{ old('nip', $usulan->id) }}" type="text" name="id"
                                                id="id" autocomplete="given-name"
                                                class="block w-full hidden  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                                        </div> {{-- {{ dd($usulan) }} --}}
                                        <div class="col-span-full flex">
                                            <label for="judul"
                                                class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                                Berkas</label>
                                            <input value="{{ old('nip', $usulan->judul) }}" disabled type="text"
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="nama"
                                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>

                                            <div style="clear:both">
                                                <iframe id="viewer" frameborder="0" scrolling="no" width="800"
                                                    height="800"
                                                    src="/assetes/{{ old('berkas', $usulan->berkas) }}"></iframe>
                                            </div>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="deskripsi"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                                            <textarea type="text" name="detail" id="detail"
                                                {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->deskripsi) }}</textarea>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="jenis"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                                File</label>
                                            <input value="{{ old('nip', $usulan->jenis) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="jenis"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                                Upload</label>
                                            <input value="{{ substr(old('nip', $usulan->updated_at), 0, 10) }}"
                                                type="text" disabled name="judul" id="judul"
                                                autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex items-center">
                                            <label for="nama"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Upload
                                                Produk Hukum</label>
                                            <input id="fileProduk" type="file" name="fileProduk"
                                                accept=".doc, .docx, .pdf" />
                                        </div>
                                        <?php
                  if (Session::get('level') == 'verifikator') {
                ?>



                                        <?php if(substr(Session::get('sub'), 0, 6) == 'Staf 2') { ?>
                                        {{-- <div class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                            <input id="berkas_ditandatangani" type="checkbox" value=""
                                                name="berkas_ditandatangani"
                                                {{ $usulan->berkas_ditandatangani == 1 ? 'checked' : '' }} type="checkbox"
                                                class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                            <label for="berkas_ditandatangani"
                                                class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                Ditandantangani</label>
                                        </div> --}}
                                        <?php } else { ?>

                                        <?php } ?>

                                        <?php
                  } else if (Session::get('level') == 'Biro Hukum') {
                ?>

                                        <div class="col-span-full flex">
                                            <label for="bidang"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                            <input value="{{ old('nip', $usulan->status) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="catatan"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                                            <textarea type="text" name="catatan" id="catatan" rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"
                                                disabled>{{ old('nip', $usulan->catatan) }}</textarea>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="catatanbiro"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan
                                                Biro<br>Hukum</label>
                                            <textarea type="text" name="catatanbiro" id="catatanbiro" rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->berkas_catatan) }}</textarea>
                                        </div>

                                        <div class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                            <input id="berkas_ditandatangani" type="checkbox" value=""
                                                name="berkas_ditandatangani"
                                                {{ $usulan->berkas_ditandatangani == 1 ? 'checked' : '' }} type="checkbox"
                                                class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                            <label for="berkas_ditandatangani"
                                                class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                Ditandantangani</label>
                                        </div>

                                        <?php
                  } else {
                ?>

                                        <div class="col-span-full flex">
                                            <label for="bidang"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                            <input value="{{ old('nip', $usulan->status) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>


                                        <?php
                  }
                ?>
                                        <?php
                    if (old('nip', $usulan->status) == 'Berkas Dikembalikan') {
                    ?>
                                        <?php
                    } else {
                    ?>



                                        <?php
                    }
                    ?>
                                    </div>
                                </div>

                                <?php
          session(['pengembalianBerkas' => "tidak"]);
          if (Session::get('level') == 'verifikator' or (old('nip', $usulan->status) == 'Berkas Dikembalikan') or (Session::get('level') == 'Biro Hukum' )) {
        ?>



                                <?php
          } else {
          }
        ?> <div class="mt-6 flex items-center justify-end gap-x-2 mb-11">
                                    <button type="submit"
                                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm">
                                        SIMPAN
                                    </button>
                                    <div class="mt-10 flex items-center justify-end gap-x-2 mb-11">
                                        <a href="{{ route('usulan.index') }}">
                                            <button type="submit"
                                                class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm">
                                                KEMBALI
                                            </button>
                                        </a>
                                    </div>
                            </form>
        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
