@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class=" h-screen px-12">
        <img class="right-14 top-6 absolute h-18 w-16" src="/images/logo_prov_kaltim.png" alt=""></img>
        <img class="right-32 top-8 absolute h-16 w-auto" src="/images/logo_bapenda_kaltim.png" alt=""></img>
        <div>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-md">
                    USULAN</h1>
            </div>

            <?php
      if (Session::get('level') == 'bidang' or Session::get('level') == 'uptd') {
      ?>

            <form action="{{ route('usulan.store') }}" method="POST" enctype="multipart/form-data"
                class="-mx-4 ring-1 ring-gray-300 sm:mx-0 sm:rounded-md bg-white p-6 mb-10">
                @csrf

                <div class="space-y-12">
                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                        <div class="col-span-full flex">
                            <label for="judul" class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                Usulan</label>
                            <input type="text" name="judul" id="judul" autocomplete="given-name"
                                class="block w-full p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="nama"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas</label>
                            <input id="file" type="file" name="file" accept=".doc, .docx, .pdf" />
                        </div>

                        <div class="col-span-full flex">
                            <label for="deskripsi"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                            <textarea type="text" name="deskripsi" id="deskripsi" rows="5"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"></textarea>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                File</label>
                            <select id="jenis" name="jenis" value="user"
                                class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                <?php

                                if (Session::get('level') == 'bidang') {
                                    echo '
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="Perda">Perda</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="Pergub">Pergub</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Gubernur">SK Gubernur</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Sekda">SK Sekda</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Kaban">SK Kaban</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ';
                                } elseif (Session::get('level') == 'uptd') {
                                    echo '
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Gubernur">SK Gubernur</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Sekda">SK Sekda</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <option value="SK Kaban">SK Kaban</option>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                Upload</label>
                            <input value="{{ date('Y-m-d') }}" type="text" disabled name="judul" id="judul"
                                autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="col-span-full flex">
                            <label for="jenis"
                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tahun</label>
                            <input value="" type="text" name="tahun" id="tahun" autocomplete="given-name"
                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1     ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                        </div>

                        <div class="flex items-center pl-4 border border-gray-200 rounded ">
                            <input id="berkas_dikirim" type="checkbox" value="" name="berkas_dikirim"
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                            <label for="berkas_dikirim" class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                Sudah Dikirim</label>
                        </div>

                    </div>
                </div>

                <div class="mt-6 flex items-center justify-end gap-x-2">
                    <button type="submit"
                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 ">Simpan</button>
                </div>


            </form>

            <?php
          } else {
        ?>

            <?php
          }
        ?>
            <form method="get" action="{{ route('cariUsulanDetail') }}" class="">
                <div class="flex items-center gap-x-3 pb-6 ">
                    <label for="photo" class="block text-sm font-bold text-black">JUDUL USULAN</label>
                    <input type="text" name="cari" id="cari"
                        class="block w-full p-2 rounded-full border-0 py-1.5 text-black shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6">
                    <button type="submit"
                        class="inline-flex items-center rounded-full bg-[#195243] px-8 py-2 text-sm font-bold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                        Cari
                    </button>
                </div>
            </form>
        </div>
        <div
            class="block w-full bg-white mb-20 p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
            <table class="min-w-full divide-y divide-gray-300 ">
                <thead>
                    <tr>
                        <th scope="col" class="py-3 pl-5 pr-3 text-left text-sm font-semibold text-black">Judul</th>
                        <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-black">Deskripsi</th>
                        <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Jenis</th>
                        <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Status</th>
                        <th scope="col" class="px-3 py-3.5 text-center text-sm font-semibold text-black">Pengaturan</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($selects as $data)
                        <tr class="border-b transition duration-300 ease-in-out items-center">
                            <td class="relative py-3 pl-5 pr-3 text-sm w-2/12">{{ $data->judul }}</td>
                            <td class="px-3 py-3.5 text-sm text-black">{{ $data->deskripsi }}</td>
                            <td class="px-3 py-3.5 text-sm text-black text-center">{{ $data->jenis }}</td>
                            <td class=" px-3 py-3.5 text-sm text-black text-center whitespace-nowrap">
                                <a
                                    class="text-xs  uppercase
              {{ $data->status == 'Verifikasi'
                  ? 'bg-orange-600 text-white rounded-full px-2 py-3'
                  : ($data->status == 'Selesai'
                      ? 'bg-[#195243] text-white rounded-full px-4 py-3'
                      : ($data->status == 'Sedang Diproses'
                          ? 'bg-blue-900 text-white rounded-full px-4 py-3'
                          : ($data->status == 'Berkas Dikembalikan'
                              ? 'bg-red-700 text-white rounded-full p-3'
                              : ''))) }}">
                                    {{ $data->status == 'Verifikasi' ? 'Ajukan Verifikasi' : $data->status }}
                                </a>
                            </td>
                            <td class=" py-3.5 pl-3 pr-4 text-center text-sm font-medium  justify-center">
                                <form method="get" action="{{ route('usulanDetail') }}" class="items-center">
                                    <input type="text" name="id_usulan" id="id_usulan" value="{{ $data->id }}"
                                        hidden />
                                    <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}"
                                        hidden />
                                    <input type="text" name="status" id="status" value="{{ $data->status }}"
                                        hidden />
                                    <button type="submit"
                                        class="inline-flex items-center uppercase font-bold rounded-full bg-[#195243] px-5 py-3 text-xs whitespace-nowrap font-semibold text-white shadow-sm ring-1 ring-inset ring-gray-300">
                                        <?php
                if ($data->status == 'Berkas Dikembalikan') {
                ?>
                                        {{ Session::get('level') == 'verifikator' ? 'Verifikasi' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Upload Ulang') }}
                                        <?php
                } else if ($data->status == 'Selesai') {
                ?>
                                        {{ Session::get('level') == 'verifikator' ? 'Edit Berkas' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Upload Ulang') }}
                                        <?php
                } else {
                ?>
                                        {{ Session::get('level') == 'verifikator' ? 'Verifikasi' : (Session::get('level') == 'Biro Hukum' ? 'Terbitkan' : 'Detail') }}
                                        <?php
                }
                ?>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="bg-red-500 text-white p-3  rounded shadow-sm">
                            <?php
                if (Session::get('level') == 'verifikator') {
                ?>
                            Semua usulan telah di Verifikasi
                            <?php
                } else {
                ?>
                            Data belum tersedia! tambah {{ basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) }}
                            <?php
                }
                ?>
                        </div>
                    @endforelse

                    <!-- More plans... -->
                </tbody>
            </table>
        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
    <script type="text/javascript">
        function PreviewImage() {
            pdffile = document.getElementById("file").files[0];
            pdffile_url = URL.createObjectURL(pdffile);
            $('#viewer').attr('src', pdffile_url);
        }
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
@endsection
