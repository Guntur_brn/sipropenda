@extends('layouts.navbar', ['title' => 'Dashboard'])

@section('content')
    <main class="bg-bgColor h-screen px-12">
        <div>
            <div class="hidden sm:-my-px sm:flex py-6 justify-between">
                <h1 class="text-4xl font-black font-sans leading-tight tracking-tight text-[#195243] py-4  rounded-md">UPLOAD
                    ULANG BERKAS</h1>
            </div>

            @forelse ($datas as $usulan)
            @empty
            @endforelse

            <div class="mb-6">
                <dl class="gap-0 flex rounded-l">
                    <div
                        class="overflow-hiden rounded-md
           bg-[#195243] p-5 shadow flex justify-between items-center col-span-2">
                        <dt class="text-xl text-center w-full  font-bold text-white">Upload<br>Berkas</dt>
                    </div>
                    <div class="overflow-hiden bg-[#195243] h-2 w-20 mt-12">
                    </div>
                    <div
                        class="overflow-hidden rounded-md
            bg-[#195243]
            p-5 shadow flex justify-between items-center col-span-2">
                        <dt class="text-xl text-center w-full  font-bold text-white">Proses<br>Verifikasi</dt>
                    </div>
                    <div
                        class="overflow-hiden
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
          bg-gray-400 h-2 w-20 mt-12">
                    </div>
                    <div
                        class="overflow-hidden rounded-md
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-3">
                        <dt class="text-xl text-center w-full  font-bold text-white">Berkas<br>Dikembalikan</dt>
                    </div>
                    <div
                        class="overflow-hiden
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-20 mt-12">
                    </div>
                    <div
                        class="overflow-hidden rounded-md
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-[#195243]' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                        <dt class="text-xl text-center w-full  font-bold text-white">Sedang<br>Diproses</dt>
                    </div>
                    <div
                        class="overflow-hiden
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            h-2 w-24 mt-12">
                    </div>
                    <div
                        class="overflow-hidden rounded-md
            {{ old('nip', $usulan->status) == 'Verifikasi' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Sedang Diproses' ? 'bg-gray-400' : '' }}
            {{ old('nip', $usulan->status) == 'Selesai' ? 'bg-[#195243]' : '' }}
            p-5 shadow flex justify-between items-center col-span-2">
                        <dt class="text-xl text-center w-full  font-bold text-white">SELESAI</dt>
                    </div>
                </dl>
            </div>

            <?php
        session(['pengembalianBerkas' => 'ya']);

        if (old('nip', $usulan->jenis) == 'SK Gubernur') {
        ?>
            <form action="{{ route('skgub.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                <?php
        } else if (old('nip', $usulan->jenis) == 'SK Sekda') {
        ?>
                <form action="{{ route('sksekda.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                    <?php
        } else if (old('nip', $usulan->jenis) == 'SK Kaban') {
        ?>
                    <form action="{{ route('skkaban.update', $usulan->id) }}" method="POST" enctype="multipart/form-data">
                        <?php
        } else if (old('nip', $usulan->jenis) == 'Perda') {
        ?>
                        <form action="{{ route('perda.update', $usulan->id) }}" method="POST"
                            enctype="multipart/form-data">
                            <?php
        } else if (old('nip', $usulan->jenis) == 'Pergub') {
        ?>
                            <form action="{{ route('pergub.update', $usulan->id) }}" method="POST"
                                enctype="multipart/form-data">
                                <?php
        }
        ?>
                                @csrf
                                @method('PUT')

                                <div class="space-y-12">
                                    <div class="m grid gap-x-6 gap-y-8 flex col ">
                                        <div class="col-span-full flex">
                                            <label for="judul"
                                                class="place-self-center text-sm font-medium  text-gray-900 w-1/12">Judul
                                                Berkas</label>
                                            <input value="{{ old('nip', $usulan->judul) }}" type="text" name="judul"
                                                id="judul" autocomplete="given-name"
                                                class="block w-full  p-2 rounded-md border-0  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="nama"
                                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas
                                                Lama</label>
                                            <div style="clear:both">
                                                <iframe id="viewer" frameborder="0" scrolling="no" width="800"
                                                    height="800"
                                                    src="/assetes/{{ old('berkas', $usulan->berkas) }}"></iframe>
                                            </div>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="nama"
                                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas
                                                Baru</label>
                                            <input id="berkasBaru" type="file" name="berkasBaru"
                                                accept=".doc, .docx, .pdf" />

                                        </div>


                                        <div class="col-span-full flex">
                                            <label for="deskripsi"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                                            <textarea type="text" name="detail" id="detail"
                                                {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? '' : 'disabled' }} rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->deskripsi) }}</textarea>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="jenis"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Jenis
                                                File</label>
                                            <input value="{{ old('nip', $usulan->jenis) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="jenis"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Tanggal
                                                Upload</label>
                                            <input value="{{ substr(old('nip', $usulan->updated_at), 0, 10) }}"
                                                type="text" disabled name="judul" id="judul"
                                                autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="Catatan"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                                            <textarea type="text" name="detail" id="detail"
                                                {{ old('nip', $usulan->status) == 'Berkas Dikembalikan' ? 'disabled' : '' }} rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->catatan) }}</textarea>
                                        </div>



                                        <?php
                  if (Session::get('level') == 'verifikator') {
                ?>

                                        <div class="col-span-full flex">
                                            <label for="bidang"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                            <select id="status" name="status" value="user"
                                                class="p-2 block w-full rounded-md border-0 bg-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                                <option value="Berkas Dikembalikan">Berkas Dikembalikan</option>
                                                <option value="Sedang Diproses">Sedang Diproses</option>
                                                <option value="Selesai">Selesai</option>
                                            </select>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="catatan"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                                            <textarea type="text" name="catatan" id="catatan" rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">{{ old('nip', $usulan->catatan) }}</textarea>
                                        </div>

                                        <div class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                            <input id="berkas_diterima" type="checkbox" value=""
                                                name="berkas_diterima"
                                                {{ $usulan->berkas_diterima == 1 ? 'checked' : '' }} type="checkbox"
                                                class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                            <label for="berkas_diterima"
                                                class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                Diterima</label>
                                        </div>

                                        <div class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                            <input id="berkas_diteruskan" type="checkbox" value=""
                                                name="berkas_diteruskan"
                                                {{ $usulan->berkas_diteruskan == 1 ? 'checked' : '' }} type="checkbox"
                                                class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                            <label for="berkas_diteruskan"
                                                class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                Diteruskan</label>
                                        </div>

                                        <?php
                  } else if (Session::get('level') == 'Biro Hukum') {
                ?>

                                        <div class="col-span-full flex">
                                            <label for="bidang"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                            <input value="{{ old('nip', $usulan->status) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="catatan"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Catatan</label>
                                            <textarea type="text" name="catatan" id="catatan" rows="5"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6"
                                                disabled>{{ old('nip', $usulan->catatan) }}</textarea>
                                        </div>

                                        <div class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                            <input id="berkas_ditandatangani" type="checkbox" value=""
                                                name="berkas_ditandatangani"
                                                {{ $usulan->berkas_ditandatangani == 1 ? 'checked' : '' }} type="checkbox"
                                                class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                            <label for="berkas_ditandatangani"
                                                class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                Ditandantangani</label>
                                        </div>

                                        <?php
                  } else {
                ?>
                                        <div class="col-span-full flex">
                                            <label for="nama"
                                                class="place-self-top w-1/12 text-sm font-medium leading-6 text-gray-900">Berkas
                                                Catatan Verifikator</label>
                                            <div style="clear:both">
                                                <iframe id="viewer" frameborder="0" scrolling="no" width="800"
                                                    height="800"
                                                    src="/assetes/{{ old('berkas', $usulan->berkas_catatan) }}"></iframe>
                                            </div>
                                        </div>

                                        <div class="col-span-full flex">
                                            <label for="bidang"
                                                class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Status</label>
                                            <input value="{{ old('nip', $usulan->status) }}" type="text" disabled
                                                name="judul" id="judul" autocomplete="given-name"
                                                class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                        </div>

                                        <?php
                  }
                ?>

                                        <hr />

                                        <?php
                    if (old('nip', $usulan->status) == 'Berkas Dikembalikan') {
                    ?>
                                        <?php
                    } else {
                    ?>
                                        <div class="hidden sm:-my-px sm:flex py-2 justify-between">
                                            <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900">Status
                                                Berkas</h1>
                                        </div>

                                        <div>
                                            <div
                                                class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                <input disabled {{ $usulan->berkas_dikirim == 1 ? 'checked' : '' }}
                                                    type="checkbox"
                                                    class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                <label class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                    Dikirim</label>
                                            </div>

                                            <div
                                                class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                <input disabled {{ $usulan->berkas_diterima == 1 ? 'checked' : '' }}
                                                    type="checkbox"
                                                    class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                <label class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                    Diterima</label>
                                            </div>

                                            <div
                                                class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                <input disabled {{ $usulan->berkas_diteruskan == 1 ? 'checked' : '' }}
                                                    type="checkbox"
                                                    class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                <label class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                    Diteruskan</label>
                                            </div>

                                            <div
                                                class="flex items-center pl-4 my-1 bg-white border border-gray-300 rounded ">
                                                <input disabled {{ $usulan->berkas_ditandatangani == 1 ? 'checked' : '' }}
                                                    type="checkbox"
                                                    class="w-4 h-4 bg-gray-100 border-gray-300 rounded focus:ring-blue-500">
                                                <label class="w-full py-4 ml-2 text-sm font-medium text-gray-900 ">Berkas
                                                    Ditandatangani</label>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="hidden sm:-my-px sm:flex py-2 justify-between">
                                            <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900">
                                                Pengupload Berkas</h1>
                                        </div>

                                        <?php
                                        $pengupload = DB::table('users')
                                            ->latest()
                                            ->where('nip', old('nip', $usulan->nip))
                                            ->get(); ?>


                                        @forelse ($pengupload as $data)
                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">NIP</label>
                                                <input value="{{ old('nip', $data->nip) }}" type="text" disabled
                                                    name="judul" id="judul" autocomplete="given-name"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Nama</label>
                                                <input value="{{ old('nip', $data->name) }}" type="text" disabled
                                                    name="judul" id="judul" autocomplete="given-name"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Level</label>
                                                <input value="{{ old('nip', $data->level) }}" type="text" disabled
                                                    name="judul" id="judul" autocomplete="given-name"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Bidang</label>
                                                <input value="{{ old('nip', $data->bidang) }}" type="text" disabled
                                                    name="judul" id="judul" autocomplete="given-name"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                            </div>

                                            <div class="col-span-full flex">
                                                <label for="bidang"
                                                    class="place-self-center w-1/12 text-sm font-medium leading-6 text-gray-900">Sub
                                                    Bidang</label>
                                                <input value="{{ old('nip', $data->subbidang) }}" type="text" disabled
                                                    name="judul" id="judul" autocomplete="given-name"
                                                    class="block w-full   p-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400  sm:text-sm sm:leading-6">
                                            </div>

                                        @empty
                                        @endforelse

                                        <?php
                    }
                    ?>
                                    </div>
                                </div>

                                <?php
          if (Session::get('level') == 'verifikator' or (old('nip', $usulan->status) == 'Berkas Dikembalikan') or (Session::get('level') == 'Biro Hukum' )) {
        ?>

                                <div class="mt-6 flex items-center justify-end gap-x-2 mb-11">
                                    <button type="submit"
                                        class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm">
                                        SIMPAN
                                    </button>

                                    <?php
          } else {
          }
        ?>
                                    <div class="mt-10 flex items-center justify-end gap-x-2 mb-11">
                                        <a href="{{ route('usulan.index') }}">
                                            <button type="submit"
                                                class="rounded-md bg-[#195243] px-3 py-2 text-sm font-semibold text-white shadow-sm">
                                                KEMBALI
                                            </button>
                                        </a>
                                    </div>
                            </form>
        </div>
        </div>
    </main>

    <script src="https://cdn.ckeditor.com/4.12.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
