/** @type {import('tailwindcss').Config} */
export default {
    content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],

  theme: {
    extend: {
      colors:{
        bgColor : '#f4f4f5',
        [#195243] : '#195243',
        hoverColor : '#fff'
    },
      fontSize: {
      xl: '2rem',
      '5xl': '2rem',
      '6xl': '3rem',
      '7xl': '5rem',
    }
    },
  },
  plugins: [],
}
