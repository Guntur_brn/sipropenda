<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Usulan;
use App\Models\Perda;
use Session;


class perdaController extends Controller
{

    public function destroy($id)
    {
        $usulan = usulan::findOrFail($id);
        $usulan->delete();

        if ($usulan) {
            //redirect dengan pesan sukses
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $usulan
     * @return void
     */

    public function uploadProdukPerda(Request $request, perda $perda){


    }

    public function update(Request $request, Perda $perda)
    {
        // menambahkan link terkait
        if (($request->link != null)) {

            $perda->update([
                'link_terkait' => $request->link
            ]);

            return redirect()->route('usulan.index');
            echo ('1');
        } else {
            if($request->status == 'Selesai') {
                    // return $request->all();
                $file = $request->fileProduk;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->fileProduk->move('assetes', $filename);

                 DB::table('file_perda')
                ->where('id', $request->id)  // find your user by their email
                    ->update( [ 'berkas' => $filename, 'nomor_surat' => $request->nomor_surat]);

        return redirect()->route('usulan.index');
            } else  if ($request->status == 'Berkas Dikembalikan') {
                $file = $request->berkasCatatan;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->berkasCatatan->move('assetes', $filename);


                $perda->update([
                    'berkas_catatan' => $filename,
                    'tgl_dikembalikan' => date(now()),
                    'status' => 'Berkas Dikembalikan',
                 ]);

                return redirect()->route('usulan.index');
                echo ('2');
            } else {
                // echo ('cek ');
                // echo (Session::get('level'));

                if (Session::get('level') == 'Biro Hukum') {
                    // echo ('ada ');
                    //     dd(Session::get('level'));

                    if ($request->has('berkas_ditandatangani')) {
                        // dd('berkas ditandatangani');
                        $perda->update([
                            'berkas_catatan' => $request->catatanbiro,
                            'berkas_ditandatangani' => '1',
                            // return redirect()->route('usulan.index');
                            'tgl_dittd' => date(now()),
                            'status' => 'Selesai'
                        ]);
                        return redirect()->route('usulan.index');
                        echo ('3');

                    } else {
                        $perda->update([
                            'berkas_catatan' => $request->catatanbiro,
                        ]);
                        return redirect()->route('usulan.index');
                        echo ('4');
                    }
                } else {
                    if (Session::get('level') == 'verifikator') {
                        $perda->update([
                            'status' => $request->status,
                            'catatan' => $request->catatan,
                        ]);

                        if ($request->has('berkas_diterima')) {
                            $perda->update([
                                'berkas_diterima' => '1',
                                'tgl_diterima' => date(now())
                            ]);
                        } else {
                            $perda->update([
                                'berkas_diterima' => '0',
                                'tgl_diterima' => null
                            ]);
                        }

                        if ($request->has('berkas_diteruskan')) {
                            $perda->update([
                                'berkas_diteruskan' => '1',
                            ]);
                        } else {
                            $perda->update([
                                'berkas_diteruskan' => '0',
                            ]);
                        }

                        return redirect()->route('usulan.index');
                        echo ('5');

                    } else if (Session::get('pengembalianBerkas') == 'ya') {
                        $file = $request->berkasBaru;

                        $filename = time() . '.' . $file->getClientOriginalExtension();
                        $request->berkasBaru->move('assetes', $filename);

                        $data = new perda();

                        $perda->update([
                            'berkas_perbaikan' => $filename,
                            'judul' => $request->judul,
                            'deskripsi' => $request->deskripsi,
                            'status' => 'Verifikasi',
                            'detail' => $request->detail,
                            'tgl_dikirim' => date(now()),
                            'tgl_dikembalikan' => null
                        ]);
                    } else {

                        //berkas ditandatangani / biro hukum
                        echo ('6');
                    }
                }
            }
        }

        return redirect()->route('usulan.index');
        // echo ('7');
    }
}
