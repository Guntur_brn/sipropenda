<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Resume;

use App\Exports\ResumeExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExportController extends Controller
{
    public function index()
    {
        $resume = Resume::all();
        return view('resume', ['resume' => $resume]);
    }

    public function export_excel()
    {
        return Excel::download(new ResumeExport, 'resume.xlsx');
    }
}