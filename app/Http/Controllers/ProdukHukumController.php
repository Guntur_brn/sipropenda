<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\ProdukHukum;
use Session;


class ProdukHukumController extends Controller
{

    public function index()
    {
        $selects = DB::table('file_produk_hukum')
            ->get();

        return view('produkHukum.index', compact('selects'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('produkHukum.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        print_r($request->post());
        // echo ($request["store"]);
        $data = new ProdukHukum();

        $file = $request->file;

        $filename = time() . '.' . $file->getClientOriginalExtension();
        $request->file->move('assetes', $filename);

        $data->berkas = $filename;
        $data->nomor_surat = $request->noSurat;
        $data->judul = $request->judul;
        $data->deskripsi = $request->deskripsi;
        $data->jenis = $request->jenis;
        $data->berkas_catatan = '';
        $data->link_terkait = '';


        $data->save();
        return redirect()->route('produkHukum.index');
    }

    /**
     * edit
     *
     * @param  mixed $produkHukum
     * @return void
     */
    public function edit(request $request)
    {
        echo ($request["id"]);
        print_r($request->post());

        // $datas = ProdukHukum::latest()->when(request()->search, function ($data_dasars) {
        //     $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
        // })
        //     ->where('id', $_GET['id_usulan'])
        //     ->get();

        // session(['jenis' => $request->jenis]);

        // if (Session::get('level') == "verifikator") {
        //     return view('produkHukum.edit', compact('datas'));
        // } else if (Session::get('level') == "Biro Hukum") {
        //     return view('produkHukum.edit', compact('datas'));
        // } else {
        //     return view('produkHukum.detail', compact('datas'));
        // }
    }

    public function detailProdukHukum(Request $request)
    {
        $datas = ProdukHukum::latest()->when(request()->search, function ($data_dasars) {
            $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id . '%');
        })
            ->where('id', $_GET['id'])
            ->get();

        return view('produkHukum.edit', compact('datas'));
    }


    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $produkHukum = produkHukum::findOrFail($id);
        $produkHukum->delete();

        if ($produkHukum) {
            //redirect dengan pesan sukses
            return redirect()->route('produkHukum.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('produkHukum.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $produkHukum
     * @return void
     */
    public function update(Request $request, produkHukum $produkHukum)
    {
        print_r($request->post());

        if ($request->has('file')) {
            $file = $request->file;

            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assetes', $filename);

            $produkHukum->update([
                'berkas' => $filename,
            ]);
        }

        $produkHukum->update([
            'nomor_surat' => $request->noSurat,
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'jenis' => $request->jenis,
        ]);
        return redirect()->route('produkHukum.index');
    }
}