<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Usulan;
use App\Models\Skkaban;
use Session;


class skkabanController extends Controller
{

    public function destroy($id)
    {
        $usulan = usulan::findOrFail($id);
        $usulan->delete();

        if ($usulan) {
            //redirect dengan pesan sukses
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $usulan
     * @return void
     */
    public function update(Request $request, skkaban $skkaban)
    {

        // menambahkan link terkait
        if ($request->link != null) {

            $skkaban->update([
                'link_terkait' => $request->link
            ]);

            return redirect()->route('usulan.index');

        } else {
            if($request->status == 'Selesai') {
                    // return $request->all();
                $file = $request->fileProduk;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->fileProduk->move('assetes', $filename);

                 DB::table('file_skkaban')
                ->where('id', $request->id)  // find your user by their email
                    ->update( [ 'berkas' => $filename, 'nomor_surat' => $request->nomor_surat]);

        return redirect()->route('usulan.index');
            } else
            if ($request->status == 'Berkas Dikembalikan') {
                $file = $request->berkasCatatan;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->berkasCatatan->move('assetes', $filename);


                $skkaban->update([
                    'berkas_catatan' => $filename,
                    'tgl_dikembalikan' => date(now()),
                    'status' => 'Berkas Dikembalikan'
                ]);

                return redirect()->route('usulan.index');

            } else {
                if (Session::get('level') == 'verifikator') {
                    $skkaban->update([
                        'status' => $request->status,
                        'catatan' => $request->catatan,
                    ]);

                    if ($request->has('berkas_diterima')) {
                        $skkaban->update([
                            'berkas_diterima' => '1',
                            'tgl_diterima' => date(now())
                        ]);
                    } else {
                        $skkaban->update([
                            'berkas_diterima' => '0',
                            'tgl_diterima' => null
                        ]);
                    }

                    if ($request->has('berkas_diteruskan')) {
                        $skkaban->update([
                            'berkas_diteruskan' => '1',
                        ]);
                    } else {
                        $skkaban->update([
                            'berkas_diteruskan' => '0',
                        ]);
                    }

                    return redirect()->route('usulan.index');

                } else if (Session::get('pengembalianBerkas') == 'ya') {
                    $file = $request->berkasBaru;

                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $request->berkasBaru->move('assetes', $filename);

                    $data = new skkaban();

                    $skkaban->update([
                        'berkas_perbaikan' => $filename,
                        'judul' => $request->judul,
                        'deskripsi' => $request->deskripsi,
                        'status' => 'Verifikasi',
                        'detail' => $request->detail,
                        'tgl_dikirim' => date(now()),
                        'tgl_dikembalikan' => null
                    ]);
                } else {
                    //berkas ditandatangani / biro hukum
                    if ($request->has('berkas_ditandatangani')) {
                        $skkaban->update([
                            'berkas_catatan' => $request->catatanbiro,
                            'berkas_ditandatangani' => '1',
                            'tgl_dittd' => date(now()),
                            'status' => 'Selesai'
                        ]);
                        echo ($request->catatanbiro);
                    } else {
                        // $skkaban->update([
                        //     'berkas_ditandatangani' => '0',
                        //     'tgl_dittd' => null,
                        // ]);
                        echo ('gagal');
                    }
                }
            }
        }

        return redirect()->route('usulan.index');
    }
}
