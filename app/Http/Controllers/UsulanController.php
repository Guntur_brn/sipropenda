<?php

namespace App\Http\Controllers;

use App\Models\ProdukHukum;
use DB;
use Illuminate\Http\Request;
use App\Models\Usulan;
use App\Models\Skgub;
use App\Models\Sksekda;
use App\Models\Perda;
use App\Models\Pergub;
use App\Models\Skkaban;
use Session;


class UsulanController extends Controller
{
    public function index()
    {
        // echo (""+Session::get('subbidang'));
        if (Session::get('subbidang') == 'kab') {
            // echo ('verifikator dan admin');
            $perda = DB::table('file_perda');
            $pergub = DB::table('file_pergub');
            $skgub = DB::table('file_skgub');
            $skkaban = DB::table('file_skkaban');
            $sksekda = DB::table('file_sksekda');

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->get();

            return view('usulan.index', compact('selects'));
        } else if (substr(Session::get('sub'), 0, 6) == 'Staf 1') {
            // echo ('staf 1');
            $perda = DB::table('file_perda');
            $pergub = DB::table('file_pergub');
            $skgub = DB::table('file_skgub');

            $selects = DB::table('file_pergub')
                ->union($perda)
                ->union($skgub)

                ->get();

            return view('usulan.index', compact('selects'));
        } else if (substr(Session::get('sub'), 0, 6) == 'Staf 2') {
            // echo ('staf 2');
            $sksekda = DB::table('file_sksekda');
            $skkaban = DB::table('file_skkaban');

            $selects = DB::table('file_sksekda')
                ->union($skkaban)
                ->union($sksekda)

                ->get();


            return view('usulan.index', compact('selects'));
        } else if (Session::get('level') == 'Biro Hukum') {
            // echo ('lainnya');
            $perda = DB::table('file_perda')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0');
            $pergub = DB::table('file_pergub')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0');
            $skgub = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0');

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->where('berkas_diteruskan', '1')
                ->where('berkas_ditandatangani', '0')
                ->get();

            return view('usulan.index', compact('selects'));
        } else {
            // echo ('lainnya');
            $perda = DB::table('file_perda')->where('nip', Session::get('nip'));
            $pergub = DB::table('file_pergub')->where('nip', Session::get('nip'));
            $skgub = DB::table('file_skgub')->where('nip', Session::get('nip'));
            $skkaban = DB::table('file_skkaban')->where('nip', Session::get('nip'));
            $sksekda = DB::table('file_sksekda')->where('nip', Session::get('nip'));

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->where('nip', Session::get('nip'))
                ->get();

            return view('usulan.index', compact('selects'));
        }
    }

    public function usulanAdmin()
    {
        $perda = DB::table('file_perda');
        $pergub = DB::table('file_pergub');
        $skgub = DB::table('file_skgub');
        $skkaban = DB::table('file_skkaban');
        $sksekda = DB::table('file_sksekda');

        $selects = DB::table('file_skgub')
            ->union($perda)
            ->union($pergub)
            ->union($skgub)
            ->union($skkaban)
            ->union($sksekda)
            ->get();

        return view('usulan.adminIndex', compact('selects'));
    }

    public function cariUsulanAdmin()
    {
        $perda = DB::table('file_perda')->where('judul', 'like', "%" . $_GET['cari'] . "%");
        $pergub = DB::table('file_pergub')->where('judul', 'like', "%" . $_GET['cari'] . "%");
        $skgub = DB::table('file_skgub')->where('judul', 'like', "%" . $_GET['cari'] . "%");
        $skkaban = DB::table('file_skkaban')->where('judul', 'like', "%" . $_GET['cari'] . "%");
        $sksekda = DB::table('file_sksekda')->where('judul', 'like', "%" . $_GET['cari'] . "%");

        $selects = DB::table('file_skgub')->where('judul', 'like', "%" . $_GET['cari'] . "%")
            ->union($perda)
            ->union($pergub)
            ->union($skgub)
            ->union($skkaban)
            ->union($sksekda)
            ->get();

        return view('usulan.adminIndex', compact('selects'));
    }


    public function cariUsulanDetail()
    {
        if (Session::get('subbidang') == 'Kab') {
            // echo ('verifikator dan admin');
            $perda = DB::table('file_perda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $pergub = DB::table('file_pergub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skgub = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skkaban = DB::table('file_skkaban')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $sksekda = DB::table('file_sksekda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");

            $selects = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'])
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->get();

            return view('usulan.index', compact('selects'));
        } else if (substr(Session::get('sub'), 0, 6) == 'Staf 1') {
            // echo ('staf 1');
            $perda = DB::table('file_perda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $pergub = DB::table('file_pergub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skgub = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");

            $selects = DB::table('file_perda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'])
                ->union($perda)
                ->union($pergub)
                ->union($skgub)

                ->get();

            return view('usulan.index', compact('selects'));
        } else if (substr(Session::get('sub'), 0, 6) == 'Staf 2') {
            // echo ('staf 2');
            $sksekda = DB::table('file_sksekda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skkaban = DB::table('file_skkaban')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");

            $selects = DB::table('file_sksekda')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'])
                ->union($skkaban)
                ->union($sksekda)

                ->get();


            return view('usulan.index', compact('selects'));
        } else if (Session::get('level') == 'Biro Hukum') {
            // echo ('lainnya');
            $perda = DB::table('file_perda')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $pergub = DB::table('file_pergub')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skgub = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skkaban = DB::table('file_skkaban')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $sksekda = DB::table('file_sksekda')->where('berkas_diteruskan', '1')->where('berkas_ditandatangani', '0')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");

            $selects = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'])
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->where('berkas_diteruskan', '1')
                ->where('berkas_ditandatangani', '0')
                ->get();

            return view('usulan.index', compact('selects'));
        } else {
            // echo ('lainnya');
            $perda = DB::table('file_perda')->where('nip', Session::get('nip'))->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $pergub = DB::table('file_pergub')->where('nip', Session::get('nip'))->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skgub = DB::table('file_skgub')->where('nip', Session::get('nip'))->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $skkaban = DB::table('file_skkaban')->where('nip', Session::get('nip'))->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");
            $sksekda = DB::table('file_sksekda')->where('nip', Session::get('nip'))->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'] . "%");

            $selects = DB::table('file_skgub')->where('berkas_diteruskan', '1')->where('judul', 'like', "%" . $_GET['cari'])
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->where('nip', Session::get('nip'))
                ->get();

            return view('usulan.index', compact('selects'));
        }
    }

    public function cariusulan(Request $request)
    {
        $keyword = $request->search;
        $datas = Usulan::where('judul', 'like', "%" . 'like', "%" . $keyword . "%");
        return view('dataDasar.index', compact('datas'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('usulan.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        print_r($request->post());
        // echo ($request["store"]);

        if ($request->jenis == 'SK Gubernur') {
            $data = new Skgub();
        } else if ($request->jenis == 'SK Sekda') {
            $data = new Sksekda();
        } else if ($request->jenis == 'SK Kaban') {
            $data = new Skkaban();
        } else if ($request->jenis == 'Perda') {
            $data = new Perda();
        } else if ($request->jenis == 'Pergub') {
            $data = new Pergub();
        } else {
            // $data = new Usulan();
        }

        $file = $request->file;

        $filename = time() . '.' . $file->getClientOriginalExtension();
        $request->file->move('assetes', $filename);

        $data->berkas = $filename;
        $data->judul = $request->judul;
        $data->deskripsi = $request->deskripsi;
        $data->jenis = $request->jenis;
        $data->status = 'Verifikasi';
        $data->catatan = $request->catatan;
        $data->tahun = $request->tahun;
        $data->nip = Session::get('nip');
        $data->tgl_dikirim = date(now());
        if ($request->has('berkas_dikirim')) {
            $data->berkas_dikirim = '1';
        }

        $data->save();
        return redirect()->back();
    }

    /**
     * edit
     *
     * @param  mixed $usulan
     * @return void
     */
    public function edit(request $request)
    {
        // echo ($request->jenis);
        if ($request->jenis == "SK Gubernur") {
            $datas = Skgub::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
        } else if ($request->jenis == "SK Sekda") {
            $datas = Sksekda::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
        } else if ($request->jenis == "Perda") {
            $datas = Perda::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
        } else if ($request->jenis == "Pergub") {
            $datas = Pergub::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
        } else if ($request->jenis == "SK Kaban") {
            $datas = Skkaban::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
        } else {
            // $data = new Usulan();
            $namaFile = "tidak ada jenis";
            echo ("tidak ada");
        }

        // session(['jenis' => $request->jenis]);

        // try {
        //     if ($_GET['status'] == "Berkas Dikembalikan") {
        //         return view('usulan.pengembalianBerkas', compact('datas'));
        //     }
        // } catch (SomeException $ignored) {
        //     // do nothing... php will ignore and continue
        //     // but maybe use "ignored" as name to silence IDE warnings.
        // }

        if (Session::get('level') == "verifikator") {
            return view('usulan.edit', compact('datas'));
        } else if (Session::get('level') == "Biro Hukum") {
            return view('usulan.edit', compact('datas'));
        } else {
            return view('usulan.detail', compact('datas'));
        }
    }

    public function usulanDetail(Request $request)
    {
        // echo ($request->jenis);
        if ($_GET['status'] == null) {
            $datas = ProdukHukum::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
            // dd($datas);
        }
         else {
            if ($request->jenis == "SK Gubernur") {
                $datas = Skgub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
               if($request->status == "Selesai") {
            return view('usulan.uploadProdukHukum', compact('datas'));
               }
            } else if ($request->jenis == "SK Sekda") {
                $datas = Sksekda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
               if($request->status == "Selesai") {
            return view('usulan.uploadProdukHukum', compact('datas'));
               }
            } else if ($request->jenis == "Perda") {
                 $datas = Perda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
               if($request->status == "Selesai") {
            return view('usulan.uploadProdukHukum', compact('datas'));
               }
            } else if ($request->jenis == "Pergub") {
                $datas = Pergub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
               if($request->status == "Selesai") {
            return view('usulan.uploadProdukHukum', compact('datas'));
               }
            } else if ($request->jenis == "SK Kaban") {
                $datas = Skkaban::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
               if($request->status == "Selesai") {
            return view('usulan.uploadProdukHukum', compact('datas'));
               }
            }

        }
        // session(['jenis' => $request->jenis]);

        // try {
        //     if ($_GET['status'] == "Berkas Dikembalikan") {
        //         return view('usulan.pengembalianBerkas', compact('datas'));
        //     }
        // } catch (SomeException $ignored) {
        //     // do nothing... php will ignore and continue
        //     // but maybe use "ignored" as name to silence IDE warnings.
        // }

        if (Session::get('level') == "verifikator") {
            return view('usulan.edit', compact('datas'));
        } else if ($_GET['status'] == "Berkas Dikembalikan") {
            return view('usulan.pengembalianBerkas', compact('datas'));
        } else if (Session::get('level') == "Biro Hukum") {
            return view('usulan.edit', compact('datas'));
        } else {
            return view('usulan.detail', compact('datas'));
        }
    }


    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $usulan = usulan::findOrFail($id);
        $usulan->delete();

        if ($usulan) {
            //redirect dengan pesan sukses
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $usulan
     * @return void
     */
    public function update(Request $request, usulan $usulan)
    {
        print_r($request->post());



        $usulan->update([
            'status' => $request->status,
            'catatan' => $request->catatan,
            'tgl_dikembalikan' => date(now())
        ]);

        if ($request->has('berkas_diterima')) {
            $usulan->update([
                'berkas_diterima' => '1',
                'tgl_diterima' => date(now())
            ]);
        } else {
            $usulan->update([
                'berkas_diterima' => '0',
                'tgl_dikembalikan' => date(now())
            ]);
        }

        if ($request->has('berkas_diteruskan')) {
            $usulan->update([
                'berkas_diteruskan' => '1',
                'tgl_diterima' => date(now())
            ]);
        } else {
            $usulan->update([
                'berkas_diteruskan' => '0',
                'tgl_dikembalikan' => date(now())
            ]);
        }


        // return redirect()->route('usulan.index');
    }
}
