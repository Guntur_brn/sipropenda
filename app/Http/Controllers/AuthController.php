<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; // baru
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{

    public function proses_login(Request $request)
    {

        request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]
        );

        $kredensil = $request->only('username', 'password');

        if (Auth::attempt($kredensil)) {
            $user = Auth::user();

            session(['level' => $user->level]);
            session(['bidang' => $user->bidang]);
            session(['nip' => $user->nip]);
            session(['nama' => $user->name]);
            session(['subbidang' => substr($user->subbidang, 0, 3)]);
            session(['sub' => $user->subbidang]);
            session(['username' => $user->username]);
            session(['id' => $user->id]);

            echo ($user);
            if ($user->level == 'admin') {
                return redirect()->intended('usulanAdmin');
            } else if (substr($user->subbidang, 0, 3) == 'Kab') {
                return redirect()->intended('resume');
            } else if (substr($user->subbidang, 0, 3) == 'Kep') {
                return redirect()->intended('resume');
            } else if (substr($user->subbidang, 0, 3) == 'Kas') {
                return redirect()->intended('usulan');
            } else {
                return redirect()->intended('usulan');
            }
        }

        return redirect('/')
            ->withInput()
            ->withErrors(['login_gagal' => 'Username dan Password SALAH']);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('/');
    }
}