<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Usulan;
use App\Models\Skgub;
use Session;


class SkgubController extends Controller
{
    public function index()
    {
        if (Session::get('subbidang') == 'kab') {
            // echo ('verifikator dan admin');
            $perda = DB::table('file_perda');
            $pergub = DB::table('file_pergub');
            $skgub = DB::table('file_skgub');
            $skkaban = DB::table('file_skkaban');
            $sksekda = DB::table('file_sksekda');

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->get();

            return view('usulan.index', compact('selects'));
        } else if (substr(Session::get('sub'), 0, 6) == 'Staf 1') {
            // echo ('staf 1');
            $perda = DB::table('file_perda');
            $pergub = DB::table('file_pergub');
            $sksekda = DB::table('file_sksekda');

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($sksekda)
                ->get();

            return view('usulan.index', compact('selects'));
        } else if (Session::get('subbidang') == 'Staf 2') {
            // echo ('staf 2');
            $selects = Usulan::when(request()->search, function ($data) {
                $data = $data->orderBy('id', 'DESC');

            })
                ->orderBy('id', 'DESC')
                ->where('jenis', 'SK Kaban')
                ->orwhere('jenis', 'SK Gubernur')
                ->get();

            return view('usulan.index', compact('selects'));
        } else {
            // echo ('lainnya');
            $perda = DB::table('file_perda')->where('nip', Session::get('nip'));
            $pergub = DB::table('file_pergub')->where('nip', Session::get('nip'));
            $skgub = DB::table('file_skgub')->where('nip', Session::get('nip'));
            $skkaban = DB::table('file_skkaban')->where('nip', Session::get('nip'));
            $sksekda = DB::table('file_sksekda')->where('nip', Session::get('nip'));

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->where('nip', Session::get('nip'))
                ->get();

            return view('usulan.index', compact('selects'));
        }
    }

    public function usulanAdmin()
    {
        $perda = DB::table('file_perda');
        $pergub = DB::table('file_pergub');
        $skgub = DB::table('file_skgub');
        $skkaban = DB::table('file_skkaban');
        $sksekda = DB::table('file_sksekda');

        $selects = DB::table('file_skgub')
            ->union($perda)
            ->union($pergub)
            ->union($skgub)
            ->union($skkaban)
            ->union($sksekda)
            ->get();

        return view('usulan.adminIndex', compact('selects'));
    }

    public function cariusulan(Request $request)
    {
        $keyword = $request->search;
        $datas = Usulan::where('namaBarang', 'like', "%" . $keyword . "%");
        return view('dataDasar.index', compact('datas'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('usulan.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        print_r($request->post());
        // echo ($request["store"]);

        if ($request->jenis == 'SK Gubernur') {
            $data = new file_skgub();
        } else {
            // $data = new Usulan();
        }

        $file = $request->file;

        $filename = time() . '.' . $file->getClientOriginalExtension();
        $request->file->move('assetes', $filename);

        $data->berkas = $filename;
        $data->judul = $request->judul;
        $data->deskripsi = $request->deskripsi;
        $data->jenis = $request->jenis;
        $data->status = 'Verifikasi';
        $data->catatan = $request->catatan;
        $data->nip = Session::get('nip');
        $data->tgl_dikirim = date(now());
        if ($request->has('berkas_dikirim')) {
            $data->berkas_dikirim = '1';
        }

        $data->save();
        return redirect()->back();
    }

    /**
     * edit
     *
     * @param  mixed $usulan
     * @return void
     */
    public function edit(usulan $usulan)
    {
        // echo ($usulan);
        // return view('usulan.edit', compact('usulan'));

    }

    public function usulanDetail(Request $request)
    {
        if ($request->jenis == "SK Gubernur") {
            $datas = file_skgub::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })->paginate();
        } else {
            // $data = new Usulan();
            $namaFile = "tidak ada jenis";
        }

        session(['jenis' => $request->jenis]);

        if (Session::get('level') == "verifikator") {
            return view('usulan.edit', compact('datas'));
        } else {
            return view('usulan.detail', compact('datas'));
        }
    }


    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $usulan = usulan::findOrFail($id);
        $usulan->delete();

        if ($usulan) {
            //redirect dengan pesan sukses
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('usulan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $usulan
     * @return void
     */
    public function update(Request $request, skgub $skgub)
    {
        // menambahkan link terkait
        if ($request->link != null) {

            $skgub->update([
                'link_terkait' => $request->link
            ]);

            return redirect()->route('usulan.index');

        } else {
            if($request->status == 'Selesai') {
                    // return $request->all();
                $file = $request->fileProduk;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->fileProduk->move('assetes', $filename);

                 DB::table('file_skgub')
                ->where('id', $request->id)  // find your user by their email
                    ->update( [ 'berkas' => $filename, 'nomor_surat' => $request->nomor_surat]);

        return redirect()->route('usulan.index');
            } else
            if ($request->status == 'Berkas Dikembalikan') {
                $file = $request->berkasCatatan;

                $filename = time() . '.' . $file->getClientOriginalExtension();
                $request->berkasCatatan->move('assetes', $filename);


                $skgub->update([
                    'berkas_catatan' => $filename,
                    'tgl_dikembalikan' => date(now()),
                    'status' => 'Berkas Dikembalikan'
                ]);

                return redirect()->route('usulan.index');

            } else {
                if (Session::get('level') == 'verifikator') {
                    $skgub->update([
                        'status' => $request->status,
                        'catatan' => $request->catatan,
                    ]);

                    if ($request->has('berkas_diterima')) {
                        $skgub->update([
                            'berkas_diterima' => '1',
                            'tgl_diterima' => date(now())
                        ]);
                    } else {
                        $skgub->update([
                            'berkas_diterima' => '0',
                            'tgl_diterima' => null
                        ]);
                    }

                    if ($request->has('berkas_diteruskan')) {
                        $skgub->update([
                            'berkas_diteruskan' => '1',
                        ]);
                    } else {
                        $skgub->update([
                            'berkas_diteruskan' => '0',
                        ]);
                    }

                    return redirect()->route('usulan.index');

                } else if (Session::get('pengembalianBerkas') == 'ya') {
                    $file = $request->berkasBaru;

                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $request->berkasBaru->move('assetes', $filename);

                    $data = new skgub();

                    $skgub->update([
                        'berkas_perbaikan' => $filename,
                        'judul' => $request->judul,
                        'deskripsi' => $request->deskripsi,
                        'status' => 'Verifikasi',
                        'detail' => $request->detail,
                        'tgl_dikirim' => date(now()),
                        'tgl_dikembalikan' => null
                    ]);
                } else {
                    //berkas ditandatangani / biro hukum
                    if ($request->has('berkas_ditandatangani')) {
                        $skgub->update([
                            'berkas_catatan' => $request->catatanbiro,
                            'berkas_ditandatangani' => '1',
                            'tgl_dittd' => date(now()),
                            'status' => 'Selesai'
                        ]);
                        echo ($request->catatanbiro);
                    } else {
                        // $skgub->update([
                        //     'berkas_ditandatangani' => '0',
                        //     'tgl_dittd' => null,
                        // ]);
                        echo ('gagal');
                    }
                }
            }
        }

        return redirect()->route('usulan.index');
    }
}
