<?php

namespace App\Http\Controllers\Api;

use App\Models\Skkaban;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;

class SkkabanController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get posts
        $posts = Skkaban::get();

        //return collection of posts as a resource
        return new PostResource(true, 'List Data Posts', $posts);
    }
}
