<?php

namespace App\Http\Controllers\Api;

use App\Models\Sksekda;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;

class SksekdaController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get posts
        $posts = Sksekda::get();

        //return collection of posts as a resource
        return new PostResource(true, 'List Data Posts', $posts);
    }
}
