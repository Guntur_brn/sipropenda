<?php

namespace App\Http\Controllers\Api;

use App\Models\Perda;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;

class PerdaController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get posts
        $posts = Perda::get();

        //return collection of posts as a resource
        return new PostResource(true, 'List Data Posts', $posts);
    }
}
