<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengguna;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $selects = Pengguna::when(request()->search, function ($dosens) {
            $dosens = $dosens->where('nip', 'like', '%' . request()->search . '%');
        })->get();

        return view('user.index', compact('selects'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        // print_r($request->post());
        // echo ($request["store"]);
        // $this->validate($request, [
        //     'kelas' => 'required',
        //     'singkatan' => 'required'
        // ]);

        $store = Pengguna::create([
            'nip' => $request->nip,
            'name' => $request->nama,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'level' => $request->slct1,
            'bidang' => $request->slct2,
            'subbidang' => $request->slct3,
        ]);

        if ($store) {
            //redirect dengan pesan sukses
            return redirect()->route('user.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('user.index')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }


    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->new_password);
        $user->save();
        return redirect()->route('pengaturan.index');
    }


    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $user = Pengguna::findOrFail($id);
        $user->delete();

        if ($user) {
            //redirect dengan pesan sukses
            return redirect()->route('user.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('user.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }
}