<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengaturan;
use Illuminate\Support\Facades\Hash;

class PengaturanController extends Controller
{
    public function index()
    {
        $selects = Pengaturan::when(request()->search, function ($dosens) {
            $dosens = $dosens->where('nip', 'like', '%' . request()->search . '%');
        })->get();

        return view('pengaturan.index', compact('selects'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('pengaturan.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        // print_r($request->post());
        // echo ($request["store"]);
        // $this->validate($request, [
        //     'kelas' => 'required',
        //     'singkatan' => 'required'
        // ]);

        $store = Pengaturan::create([
            'nip' => $request->nip,
            'name' => $request->nama,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'level' => $request->slct1,
            'bidang' => $request->slct2,
            'subbidang' => $request->slct3,
        ]);

        if ($store) {
            //redirect dengan pesan sukses
            return redirect()->route('pengaturan.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('pengaturan.index')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    /**
     * edit
     *
     * @param  mixed $pengaturan
     * @return void
     */
    public function edit(pengaturan $pengaturan)
    {
        // echo ($pengaturan);
        return view('pengaturan.edit', compact('pengaturan'));
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $pengaturan = pengaturan::findOrFail($id);
        $pengaturan->delete();

        if ($pengaturan) {
            //redirect dengan pesan sukses
            return redirect()->route('pengaturan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('pengaturan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }
}