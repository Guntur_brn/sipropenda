<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Usulan;
use App\Models\Skgub;
use App\Models\Sksekda;
use App\Models\Perda;
use App\Models\Pergub;
use App\Models\Skkaban;
use App\Models\ProdukHukum;
use Session;

use App\Exports\ResumeExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ResumeController extends Controller
{
    public function index()
    {
        $perda = DB::table('file_perda')->where('status','Selesai');
        $skgub = DB::table('file_skgub')->where('status','Selesai');
        $skkaban = DB::table('file_skkaban')->where('status','Selesai');
        $sksekda = DB::table('file_sksekda')->where('status','Selesai');
        $sksekda = DB::table('file_produk_hukum');

        $selects = DB::table('file_pergub')
            ->where('status','Selesai')
            ->union($perda)
            ->union($skgub)
            ->union($skkaban)
            ->union($sksekda)
            ->get();
// dd($selects);
        return view('resume.index', compact('selects'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('resume.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // echo ("request");
        // print_r($request->post());
        // echo ($request["store"]);
        // $this->validate($request, [
        //     'kelas' => 'required',
        //     'singkatan' => 'required'
        // ]);

        $store = Resume::create([
            'nip' => $request->nip,
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => $request->password,
            'leveling' => $request->leveling,
            'bidang' => $request->bidang,
        ]);

        if ($store) {
            //redirect dengan pesan sukses
            return redirect()->route('resume.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('resume.index')->with(['error' => 'Data Gagal Disimpan!']);
        }

    }

    /**
     * edit
     *
     * @param  mixed $resume
     * @return void
     */
    public function edit(resume $resume)
    {
        // echo ($resume);
        return view('resume.edit', compact('resume'));
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $resume = resume::findOrFail($id);
        $resume->delete();

        if ($resume) {
            //redirect dengan pesan sukses
            return redirect()->route('resume.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('resume.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    public function resumeDetail(Request $request)
    {
        // echo ($request->jenis);
        if ($_GET['status'] == null) {
            $datas = ProdukHukum::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
            // dd($datas);
        } else {
            if ($request->jenis == "SK Gubernur") {
                $datas = Skgub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "SK Sekda") {
                $datas = Sksekda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "Perda") {
                $datas = Perda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "Pergub") {
                $datas = Pergub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
                print($_GET['id_usulan']);
            } else if ($request->jenis == "SK Kaban") {
                $datas = Skkaban::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            }
        }

        // try {
        //     if ($_GET['status'] == "Berkas Dikembalikan") {
        //         return view('usulan.pengembalianBerkas', compact('datas'));
        //     }
        // } catch (SomeException $ignored) {
        //     // do nothing... php will ignore and continue
        //     // but maybe use "ignored" as name to silence IDE warnings.
        // }


        return view('resume.detail', compact('datas'));

    }

     public function resumeDetailPublic(Request $request)
    {
        // echo ($request->jenis);
        if ($_GET['status'] == null) {
            $datas = ProdukHukum::latest()->when(request()->search, function ($data_dasars) {
                $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
            })
                ->where('id', $_GET['id_usulan'])
                ->get();
            // dd($datas);
        } else {
            if ($request->jenis == "SK Gubernur") {
                $datas = Skgub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "SK Sekda") {
                $datas = Sksekda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "Perda") {
                $datas = Perda::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            } else if ($request->jenis == "Pergub") {
                $datas = Pergub::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
                print($_GET['id_usulan']);
            } else if ($request->jenis == "SK Kaban") {
                $datas = Skkaban::latest()->when(request()->search, function ($data_dasars) {
                    $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id_usulan . '%');
                })
                    ->where('id', $_GET['id_usulan'])
                    ->get();
            }
        }

        // try {
        //     if ($_GET['status'] == "Berkas Dikembalikan") {
        //         return view('usulan.pengembalianBerkas', compact('datas'));
        //     }
        // } catch (SomeException $ignored) {
        //     // do nothing... php will ignore and continue
        //     // but maybe use "ignored" as name to silence IDE warnings.
        // }


        return view('resume.public', compact('datas'));

    }

    public function export()
    {
        $perda = DB::table('file_perda');
        $pergub = DB::table('file_pergub');
        $skgub = DB::table('file_skgub');
        $skkaban = DB::table('file_skkaban');
        $sksekda = DB::table('file_sksekda');

        $selects = DB::table('file_skgub')
            ->union($perda)
            ->union($pergub)
            ->union($skgub)
            ->union($skkaban)
            ->union($sksekda)
            ->get();

        return view('resume.export', compact('selects'));
    }

    public function export_excel()
    {
        // return Excel::download(new ResumeExport, 'resume.xlsx');
    }

    function exportexcel()
    {
        return Excel::download(new ResumeExport, 'Resume.xlsx');
    }

    public function cariResume()
    {
        if ($_GET['aksi'] == "cari") {
            $perda = DB::table('file_perda')->where('jenis', '=', $_GET['jenis']);
            $pergub = DB::table('file_pergub')->where('jenis', '=', $_GET['jenis']);
            $skgub = DB::table('file_skgub')->where('jenis', '=', $_GET['jenis']);
            $skkaban = DB::table('file_skkaban')->where('jenis', '=', $_GET['jenis']);
            $sksekda = DB::table('file_sksekda')->where('jenis', '=', $_GET['jenis']);

            $selects = DB::table('file_skgub')
                ->union($perda)
                ->union($pergub)
                ->union($skgub)
                ->union($skkaban)
                ->union($sksekda)
                ->get()->where('jenis', 'like', $_GET['jenis']);

            return view('resume.index', compact('selects'));
        } else {
            return Excel::download(new ResumeExport, 'Resume.xlsx');
        }
    }

    public function resumePublic(){
        $jenis = $_GET['jenis'];

        if($jenis=='PD'){
            $file = DB::table('file_perda')->where('status', '=', 'Selesai');
            $selects = DB::table('file_produk_hukum')
                    ->union($file)
                    ->get()->where('jenis', '=', 'Perda');
        } else {
            $file = DB::table('file_pergub')->where('status', '=', 'Selesai');
            $selects = DB::table('file_produk_hukum')
                    ->union($file)
                    ->get()->where('jenis', '=', 'Pergub');

        }

        return view('resumepublic', compact('selects'));
    }
}
