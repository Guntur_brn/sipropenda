<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Panduan;
use Session;
use Response;


class panduanController extends Controller
{

    public function index()
    {
        // $selects = DB::table('panduan')
        //     ->get();

        $selects = Panduan::sortable()->paginate(5);

        return view('panduan.index', compact('selects'));
    }

    /**
     * create
     *
     * @return void
     */
    public function create()
    {
        return view('panduan.create');
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */

    /**
     * edit
     *
     * @param  mixed $panduan
     * @return void
     */
    public function edit(request $request)
    {
        $datas = panduan::latest()->when(request()->search, function ($data_dasars) {
            $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id . '%');
        })
            ->where('id', $_GET['id'])
            ->get();

        return view('panduan.edit', compact('datas'));
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $panduan = panduan::findOrFail($id);
        $panduan->delete();

        if ($panduan) {
            //redirect dengan pesan sukses
            return redirect()->route('panduan.index')->with(['pesan' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('panduan.index')->with(['pesan' => 'Data Gagal Dihapus!']);
        }
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $panduan
     * @return void
     */
    public function update(Request $request, panduan $panduan)
    {
        print_r($request->post());

        if ($request->has('file')) {
            $file = $request->file;

            $filename = time() . '.' . $file->getClientOriginalExtension();
            $request->file->move('assetes', $filename);

            $panduan->update([
                'panduan' => $filename,
            ]);
        }

        return redirect()->route('panduan.index');
    }

    public function detailPanduan(Request $request)
    {
        $datas = Panduan::latest()->when(request()->search, function ($data_dasars) {
            $data_dasars = $data_dasars->where('id', 'like', '%' . request()->id . '%');
        })
            ->where('id', $_GET['id'])
            ->get();

        return view('panduan.detail', compact('datas'));
    }

    public function downloadPanduan()
    {
        $file = public_path() . "/assetes/" . $_GET['id'];
        $headers = array('Content-Type: application/pdf', );
        return Response::download($file, 'panduan.pdf', $headers);
    }


}