<?php

namespace App\Exports;

use App\Models\Skgub;
use App\Models\Skkaban;
use App\Models\Sksekda;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class ResumeExport implements FromView
{
    public function view(): View
    {
        $sk_kaban = Skkaban::all();
        $sk_sekda = Sksekda::all()->union($sk_kaban);



        return view('resume.table', ['selects' => $sk_sekda]);

    }
}