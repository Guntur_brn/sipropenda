<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdukHukum extends Model
{

    use HasFactory;
    public $table = "file_produk_hukum";

    protected $fillable = [
        'nomor_surat',
        'judul',
        'berkas',
        'deskripsi',
        'jenis',
        'status',
        'catatan',
        'nip',
        'berkas_catatan',
        'link_terkait'
    ];
}