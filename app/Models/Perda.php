<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Perda extends Model
{

    use HasFactory, Sortable;
    public $table = "file_perda";

    protected $fillable = [
        'judul',
        'berkas',
        'deskripsi',
        'jenis',
        'status',
        'catatan',
        'nip',
        'berkas_dikirim',
        'berkas_diterima',
        'berkas_diteruskan',
        'berkas_ditandatangani',
        'berkas_perbaikan',
        'tgl_dikirim',
        'tgl_diterima',
        'tgl_dikembalikan',
        'berkas_catatan',
        'tgl_dittd',
        'link_terkait'
    ];

    public $sortable = [
        'judul',
        'berkas',
        'deskripsi',
        'jenis',
        'status',
        'catatan',
        'nip',
        'berkas_dikirim',
        'berkas_diterima',
        'berkas_diteruskan',
        'berkas_ditandatangani',
        'berkas_perbaikan',
        'tgl_dikirim',
        'tgl_diterima',
        'tgl_dikembalikan',
        'berkas_catatan',
        'tgl_dittd',
        'link_terkait'
    ];
}
