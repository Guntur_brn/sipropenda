<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    use HasFactory;
    public $table = "users";

    protected $fillable = [
        'id',
        'nip',
        'name',
        'username',
        'password',
        'level',
        'bidang',
        'subbidang'
    ];
}