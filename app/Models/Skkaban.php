<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skkaban extends Model
{

    use HasFactory;
    public $table = "file_skkaban";

    protected $fillable = [
        'judul',
        'berkas',
        'deskripsi',
        'jenis',
        'status',
        'catatan',
        'nip',
        'berkas_dikirim',
        'berkas_diterima',
        'berkas_diteruskan',
        'berkas_ditandatangani',
        'berkas_perbaikan',
        'berkas_catatan',
        'tgl_dikirim',
        'tgl_diterima',
        'tgl_dikembalikan',
        'tgl_dittd'
    ];
}