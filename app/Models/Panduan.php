<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Panduan extends Model
{

    use HasFactory, Sortable;
    public $table = "panduan";

    protected $fillable = [
        'level',
        'panduan'
    ];
}