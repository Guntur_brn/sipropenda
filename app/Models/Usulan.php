<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usulan extends Model
{

    use HasFactory;
    public $table = "file_skgub";

    protected $fillable = [
        'judul',
        'berkas',
        'deskripsi',
        'jenis',
        'status',
        'catatan',
        'nip',
        'berkas_dikirim',
        'berkas_diterima',
        'berkas_diteruskan',
        'berkas_ditandatangani',
        'tahun'

    ];
}
