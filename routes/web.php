<?php

namespace App\Http\Controllers;

use App\Models\ProdukHukum;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginRegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/loginView', function () {
    return view('login');
});

Route::get('/dashboardPublic', function () {
    return view('dashboardPublic');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/cariDashboardPublic', function () {
    return view('dashboardPublic');
});

Route::get('/caridashboard', function () {
    return view('dashboard');
});

Route::get('/password', function () {
    return view('pengaturan/password');
});

// Auth::routes();

Route::resource('pengaturan', PengaturanController::class);
Route::resource('usulan', UsulanController::class);
Route::resource('resume', ResumeController::class);
Route::resource('perda', PerdaController::class);
Route::resource('pergub', PergubController::class);
Route::resource('skgub', SkgubController::class);
Route::resource('skkaban', SkkabanController::class);
Route::resource('sksekda', SksekdaController::class);
Route::resource('user', UserController::class);
Route::resource('produkHukum', ProdukHukumController::class);
Route::resource('panduan', PanduanController::class);
Route::get('usulanAdmin', 'App\Http\Controllers\UsulanController@UsulanAdmin')->name('usulanAdmin');
Route::post('change-password', 'App\Http\Controllers\UserController@updatePassword')->name('update-password');
Route::post('uploadProdukPerda', 'App\Http\Controllers\PerdaController@uploadProdukPerda')->name('uploadProdukPerda');


Route::get('login', 'App\Http\Controllers\AuthController@index')->name('login');
Route::post('proses_login', 'App\Http\Controllers\AuthController@proses_login')->name('proses_login');
Route::get('logout', 'App\Http\Controllers\AuthController@logout')->name('logout');
Route::get('/cariusulan', [UsulanController::class, 'cariusulan'])->name('cariusulan');

Route::get('/usulanDetail', [UsulanController::class, 'usulanDetail'])->name('usulanDetail');
Route::get('/detailProdukHukum', [ProdukHukumController::class, 'detailProdukHukum'])->name('detailProdukHukum');
Route::get('/detailPanduan', [PanduanController::class, 'detailPanduan'])->name('detailPanduan');
Route::get('/resumeDetail', [ResumeController::class, 'resumeDetail'])->name('resumeDetail');
Route::get('/resumeDetailPublic', [ResumeController::class, 'resumeDetailPublic'])->name('resumeDetailPublic');
Route::get('/resumePublic', [ResumeController::class, 'resumePublic'])->name('resumePublic');
Route::get('/downloadPanduan', [PanduanController::class, 'downloadPanduan'])->name('downloadPanduan');

Route::get('/export', [ResumeController::class, 'export'])->name('export');

Route::get('/cariUsulanDetail', [UsulanController::class, 'cariUsulanDetail'])->name('cariUsulanDetail');
Route::get('/cariUsulanAdmin', [UsulanController::class, 'cariUsulanAdmin'])->name('cariUsulanAdmin');

Route::get('/exportResume', [ResumeController::class, 'exportexcel'])->name('exportResume');
Route::get('/cariResume', [ResumeController::class, 'cariResume'])->name('cariResume');
