<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/perda', App\Http\Controllers\Api\PerdaController::class);
Route::apiResource('/pergub', App\Http\Controllers\Api\PergubController::class);
Route::apiResource('/skgub', App\Http\Controllers\Api\SkgubController::class);
Route::apiResource('/sksekda', App\Http\Controllers\Api\SksekdaController::class);
Route::apiResource('/skkaban', App\Http\Controllers\Api\SkkabanController::class);
